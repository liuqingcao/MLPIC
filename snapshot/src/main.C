/***********************************************************************
 *
 *
 * 
 * 
 * 
//////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <TString.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualFFT.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TStyle.h>
#include <TPad.h>
#include <TGaxis.h>
#include <TLatex.h>
#include <gsl/gsl_fft_halfcomplex.h>

using namespace std;
int CountLines(const char *filename);
void SetStyle(TMultiGraph *h1, char *title, char *Xtitle, char *Ytitle, double xmin, double xmax);
//define parameters
	Double_t ME = 9.10938188e-31;
	Double_t Q = 1.602e-19;
	Double_t PI = 3.14159265;
	Double_t EBSN = 8.8541878e-12;
	Double_t C = 2.99792458e8;
	Double_t WAVELENGTH = 1e-6;
	Double_t T0 = WAVELENGTH/C;
	Double_t FREQUENCY = 1.0/T0;
	Double_t OMEGA = 2.0*PI*FREQUENCY;
	Double_t KAY = 2.0*PI/WAVELENGTH;
	Double_t EOVERA = ME*OMEGA*C/Q;
	Double_t NC = ME*EBSN*OMEGA*OMEGA/Q*Q;
	Double_t IOVERE = C*EBSN;

int main(int argc, char *argv[])
{	
	double low_frequency, xmin, xmax, time;
	int is_show;
	is_show = 0;
	string lowf, str_time;
	if(argc == 6){
		cout<<"snapshot...";
		
		low_frequency = atof(argv[1]);
		stringstream Str_lowf;
		Str_lowf<<low_frequency;
		Str_lowf>>lowf;

		xmin = atof(argv[2]);
		xmax = atof(argv[3]);
		is_show = atoi(argv[4]);
		
		time = atof(argv[5]);
		stringstream Str_time;
		Str_time<<time;
		Str_time>>str_time;		

	}else{
		cout<<"input wrong"<<endl;
		cout<<"Usage: mlpic-snapshot [low frequency] [X-min] [X-max]【window】[time]"<<endl;
		return 0;		
	}
	
	int filename_size = 512;
	char *path, *file;
	path = new char [filename_size];
	file = new char [filename_size];
	sprintf( path, "./" );
	sprintf( file, "%ssnap-%.3f", path, time );
	//open the file
	ifstream fp;
	fp.open(file,ios_base::in);
	if(fp.fail()){
		cout<<file<<" is not exist !"<<endl;
		return 0;
	}	
//read the data in//////////////////////////////////////////////////////
	//get the data row
	int row = CountLines(file) - 1;
	int column = 13;
	string data_name[column];
	Double_t data[row][column];
	int index=0;
	for(int i=0;i<column;i++) fp >> data_name[i];		
	while(index < row)
	{
		for(int i=0;i<column;i++){
			fp >> data[index][i];			
		}
		index++;
	}
	fp.close();
	
//handle the data///////////////////////////////////////////////////////
	Double_t x[row], ex[row], ey[row], ez[row], eyz[row], feyz[row];
	Double_t by[row], bz[row];
	Double_t ey_old[row], ez_old[row];
	Double_t jx[row], jy[row], jz[row], jyz[row], edens[row], idens[row];
	for(int i=0;i<row;i++){
		x[i] = data[i][0];
		ex[i] = data[i][1];
		ey_old[i] = ey[i] = data[i][2];
		ez_old[i] = ez[i] = data[i][3];
		eyz[i] = sqrt(ey[i]*ey[i] + ez[i]*ez[i]);
		by[row] = data[i][4];
		bz[row] = data[i][5];
		edens[i] = -1.0 * data[i][6];
		idens[i] = data[i][7];
		jx[i] = data[i][8];
		jy[i] = data[i][9];
		jz[i] = data[i][10];		
		jyz[i] = sqrt(jy[i]*jy[i] + jz[i]*jz[i]);
	}			
	//filter
	Double_t df = 1.0/(row * (x[1]-x[0]) );
	//filter Ey	
	gsl_fft_real_workspace *work_ey = gsl_fft_real_workspace_alloc(row);
	gsl_fft_real_wavetable *real_ey = gsl_fft_real_wavetable_alloc(row);
	gsl_fft_real_transform (ey, 1, row, real_ey, work_ey);
	gsl_fft_real_wavetable_free (real_ey);
	for (int i = 0; i < row && ((i+1)/2)*df < low_frequency; i++)
		ey[i] = 0;	
	gsl_fft_halfcomplex_wavetable *hc_ey = gsl_fft_halfcomplex_wavetable_alloc (row);
	gsl_fft_halfcomplex_inverse (ey, 1, row, hc_ey, work_ey);
	gsl_fft_halfcomplex_wavetable_free (hc_ey);
	gsl_fft_real_workspace_free (work_ey);
	//filter Ez
	gsl_fft_real_workspace *work_ez = gsl_fft_real_workspace_alloc(row);
	gsl_fft_real_wavetable *real_ez = gsl_fft_real_wavetable_alloc(row);
	gsl_fft_real_transform (ez, 1, row, real_ez, work_ez);
	gsl_fft_real_wavetable_free (real_ez);	
	for (int i = 0; i < row && ((i+1)/2)*df < low_frequency; i++)
		ez[i] = 0;	
	gsl_fft_halfcomplex_wavetable *hc_ez = gsl_fft_halfcomplex_wavetable_alloc (row);
	gsl_fft_halfcomplex_inverse (ez, 1, row, hc_ez, work_ez);
	gsl_fft_halfcomplex_wavetable_free (hc_ez);
	gsl_fft_real_workspace_free (work_ez);
	//Eyz
	for(int i=0;i<row;i++) feyz[i] = sqrt(ey[i]*ey[i] + ez[i]*ez[i]);	
	//get the MAX
	Double_t feyz_Max = 0.0;
	Int_t Index=0;
	for(int i=int(0.1*row);i<int(0.9*row);i++)
		if(feyz_Max < feyz[i]){
			feyz_Max = feyz[i];
			Index = i;
		}			
	//get the FWHM
	Double_t FWHM=0.0, feyz_L=0.0, feyz_R=0.0;
	Int_t Index_L, Index_R;
	Index_L = Index_R = 0;
	for(int i=int(0.2*index);i<Index;i++)
		if( feyz[i] / feyz_Max >= 0.5 ){
			feyz_L = feyz[i];
			Index_L = i;
			break;
		}	
	for(int i=Index;i<int(row*0.8);i++)
		if(  feyz[i] / feyz_Max <= 0.5 ){
			feyz_R = feyz[i];
			Index_R = i;
			break;
		}	
	FWHM = (x[Index_R]-x[Index_L])*3333.3;
	cout<<"DONE"<<endl;

//plot//////////////////////////////////////////////////////////////////
	if(is_show != 1) gROOT->SetBatch(true);
	TApplication* rootapp = new TApplication("Snapshot",&argc, argv);	
	TGaxis::SetMaxDigits(3);
	gStyle->SetTitleSize(0.065,"t");
	
	string cname="snap->" + lowf + "w-" + str_time +"T";
	TCanvas *cc = new TCanvas("cc",cname.c_str(),1400,800);
	if(is_show == 1) cc->ToggleEventStatus();
	cc->Divide(3,2);
	//eyz
	cc->cd(1);
	gPad->SetMargin(0.2,0.05,0.2,0.1);
	TMultiGraph *mg1 = new TMultiGraph();
	TGraph *geyz = new TGraph(row,x,eyz);
	geyz->SetLineColor(kRed);
	geyz->SetLineWidth(1);
	mg1->Add(geyz);
	mg1->Draw("AL");
	SetStyle(mg1,"(a) Electric Field","x/#lambda","#sqrt{ey^{2}+ez^{2}}",xmin,xmax);

	//filter eyz
	cc->cd(2);
	gPad->SetMargin(0.2,0.1,0.2,0.1);
	TMultiGraph *mg2 = new TMultiGraph();
	TGraph *gfeyz = new TGraph(row,x,feyz);
	gfeyz->SetLineColor(kRed);
	gfeyz->SetLineWidth(1);
	mg2->Add(gfeyz);
	mg2->Draw("AL");
	SetStyle(mg2,"(b) Filter Electric Field","x/#lambda","#sqrt{ey^{2}+ez^{2}}",xmin,xmax);
	mg2->GetYaxis()->SetRangeUser(0,feyz_Max*1.1);
	//latex
	TLatex text1;
	char *buffer=new char[30];
	sprintf( buffer , "%4.2f", feyz_Max );
	text1.SetTextAlign(12);
	text1.SetTextSize(0.06);
	text1.SetTextColor(kBlue);	
	text1.DrawLatex(x[Index]+0.5*df,feyz_Max,buffer);	
	TLatex text3;
	text3.SetTextAlign(12);
	text3.SetTextSize(0.06);
	text3.SetTextColor(kBlue);		
	char *buffer1=new char[30];
	sprintf( buffer1 , "%6.1f", FWHM );
	text3.DrawLatex(x[Index_R]+0.2*df,0.5*feyz_Max,buffer1);


	//electron density
	cc->cd(3);
	gPad->SetMargin(0.2,0.05,0.2,0.1);
	TMultiGraph *mg3 = new TMultiGraph();	
	TGraph *gedens = new TGraph(row,x,edens);
	gedens->SetLineColor(kBlue);
	gedens->SetLineWidth(1);	
	mg3->Add(gedens);
	mg3->Draw("AL");
	SetStyle(mg3,"   (c) Electron Density","x/#lambda","Density/n_{c}",xmin,xmax);

	//Ex
	cc->cd(4);
	gPad->SetMargin(0.2,0.05,0.2,0.1);
	TMultiGraph *mgex = new TMultiGraph();	
	TGraph *gex = new TGraph(row,x,ex);
	gex->SetLineColor(kBlue);
	gex->SetLineWidth(1);	
	mgex->Add(gex);
	mgex->Draw("AL");
	SetStyle(mgex," (d) Ex","x/#lambda","eE/m_{e}#omega_{0}c",xmin,xmax);

	//Ey
	cc->cd(5);
	gPad->SetMargin(0.2,0.1,0.2,0.1);
	TMultiGraph *mg5 = new TMultiGraph();
	TGraph *gey = new TGraph(row,x,ey_old);
	gey->SetLineColor(kBlue);
	gey->SetLineWidth(1);
	mg5->Add(gey);
	TGraph *HPey = new TGraph(row,x,ey);
	HPey->SetLineColor(kRed);
	HPey->SetLineWidth(1);	
	mg5->Add(HPey);
	mg5->Draw("AL");
	SetStyle(mg5,"(e)   Ey&Filter","x/#lambda","eE/m_{e}#omega_{0}c",xmin,xmax);

	cc->cd(6);
	gPad->SetMargin(0.2,0.1,0.2,0.1);
	TMultiGraph *mg6 = new TMultiGraph();
	TGraph *gez = new TGraph(row,x,ez_old);
	gez->SetLineColor(kBlue);
	gez->SetLineWidth(1);
	mg6->Add(gez);
	TGraph *HPez = new TGraph(row,x,ez);
	HPez->SetLineColor(kRed);
	HPez->SetLineWidth(1);	
	mg6->Add(HPez);
	mg6->Draw("AL");
	SetStyle(mg6,"(f)   Ez&Filter","x/#lambda","eE/m_{e}#omega_{0}c",xmin,xmax);

	//save
	string savename = "./snap-" + lowf + "-" + str_time + "T.gif";
	cc->SaveAs(savename.c_str());	
	if(is_show != 0) rootapp->Run();

	return 1;
}


void SetStyle(TMultiGraph *h1, char *title, char *Xtitle, char *Ytitle, double xmin, double xmax)
{
	h1->SetTitle(title);	
	
	h1->GetXaxis()->SetTitle(Xtitle);
	h1->GetXaxis()->CenterTitle();	
	h1->GetXaxis()->SetTitleSize(0.06);
	h1->GetXaxis()->SetTitleOffset(1.3);
	h1->GetXaxis()->SetLabelSize(0.06);
	h1->GetXaxis()->SetLabelOffset(0.025);
	h1->GetXaxis()->SetNdivisions(7,2,0,kTRUE);
	h1->GetXaxis()->SetRangeUser(xmin,xmax);
	
	h1->GetYaxis()->SetTitle(Ytitle);
	h1->GetYaxis()->CenterTitle();	
	h1->GetYaxis()->SetTitleSize(0.06);
	h1->GetYaxis()->SetTitleOffset(1.65);
	h1->GetYaxis()->SetLabelSize(0.06);
	h1->GetYaxis()->SetLabelOffset(0.025);
	h1->GetYaxis()->SetNdivisions(7,2,0,kTRUE);

}


//判断文件行数
int CountLines(const char *filename)
	{
		ifstream ReadFile;
		int n=0;
		char line[512];
		ReadFile.open(filename,ios_base::in);//以只读的方式读取文件
		if(ReadFile.fail())//文件打开失败:返回0
		{
			return 0;
		}
		else//文件存在
		{
			while(!ReadFile.eof())//判断文件结尾
			{
				ReadFile.getline(line,sizeof(line));
				n++;
			}
	    return n-1;//把多数的一行减掉
	    }
    ReadFile.close();
    }		
	

