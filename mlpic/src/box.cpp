#include "box.h"

box::box( parameter &p )
  : input(p),
    grid(p)       // initialize grid, cells, particles

{
  sprintf( errname, "%s/error", p.path );
  static error_handler bob("box::Constructor",errname);

  n_domains = input.n_domains;

  n_el   = grid.n_el;    // if there is more than one domain
  n_ion  = grid.n_ion;   // these numbers will be updated
  n_part = grid.n_part;  // in the following

}

//////////////////////////////////////////////////////////////////////////////////////////

input_box::input_box( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path );
  static error_handler bob("input_box::Constructor",errname);
  
  rf.openinput(p.input_file_name); 

  Q_restart      = 0;

  n_domains      = 1;

  Q_reorganize   = 1; 
  delta_reo      = 1; 

  rf.closeinput();

  nsp            = p.nsp;

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_box::save( parameter &p )
{
  static error_handler bob("input_box::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "box" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q_restart          : " << Q_restart      << endl;
  outfile << "restart_file       : " << restart_file   << endl;
  outfile << "Q_restart_save     : " << Q_restart_save << endl;
  outfile << "restart_file_save  : " << restart_file_save  << endl;
  outfile << "N_domains          : " << n_domains      << endl;
  outfile << "Q_reorganize       : " << Q_reorganize   << endl;
  outfile << "delta_reo          : " << delta_reo      << endl;
  outfile << "nsp                : " << nsp            << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}

/////////////////////////////////////////////////////////////////////////////////////////
//eof


