#ifndef DIAGNOSTIC_STEPPER_H
#define DIAGNOSTIC_STEPPER_H

#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;


typedef struct stepper_param {
  int Q;
  double t_start;                // in periods
  double t_stop;                 // in periods
  double t_step;                 // in periods
  int x_start;                   // in cells
  int x_stop;                    // in cells
  int x_step;                    // in cells
} stepper_param;


//////////////////////////////////////////////////////////////////////////////////////////


class diagnostic_stepper {
private:
  char     errname[filename_size];

public:
  int Q;

  int   t_start;
  int   t_stop;
  int   t_step;
  int   t_count;
  int   flag_pha;
  double t_count_pha;
  double t_step_pha;
  double t_start_pha;
  double t_stop_pha;

  int x_start;
  int x_stop;
  int x_step;
  
  diagnostic_stepper( stepper_param &t, parameter &p );
};  

//////////////////////////////////////////////////////////////////////////////////////////
#endif




