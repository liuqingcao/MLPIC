#include "diagnostic_stepper.h"


diagnostic_stepper::diagnostic_stepper( stepper_param &t, parameter &p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("stepper_diagnostic::Constructor",errname);

  Q = t.Q;

  t_start = (int) floor( t.t_start * p.spp + 0.5 );
  t_stop  = (int) floor( t.t_stop * p.spp + 0.5 );
  if (t.t_step<TINY) t_step = 1;
  else               t_step  = (int) floor( t.t_step * p.spp + 0.5 );
  t_count = 0;
  t_count_pha = 0.0;
   
  flag_pha = 0;
  t_start_pha = t.t_start;
  t_stop_pha  = t.t_stop;
  t_step_pha  = t.t_step;

  x_start = t.x_start;
  x_stop  = t.x_stop;
  x_step  = t.x_step;
}


//////////////////////////////////////////////////////////////////////////////////////////
//eof
