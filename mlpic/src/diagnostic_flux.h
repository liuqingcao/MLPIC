#ifndef DIAGNOSTIC_FLUX_H
#define DIAGNOSTIC_FLUX_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;


class input_flux {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper; 
  int           Q_restart;
  char          restart_file[filename_size];

  input_flux( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class flux {
private:
  char       errname[filename_size];
  readfile   rf;
  input_flux input;

public:
  diagnostic_stepper stepper;

  char     *name;
  ofstream file;

  flux  ( parameter &p );
  void write_flux ( double time, domain* grid );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
