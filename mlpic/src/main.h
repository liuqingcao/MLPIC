#ifndef MAIN_H
#define MAIN_H

#include "common.h"
#include <iostream>
#include "error.h"
#include "parameter.h"
#include "box.h"
#include "diagnostic.h"
#include "uhr.h"
#include "propagate.h"

using namespace std;

int main(int argc, char **argv);

int main_exit( parameter &p, box &sim ) 
{ 
  char                 errname[filename_size];   
  sprintf( errname, "%s/error-%d", p.path, p.domain_number );
  static error_handler bob("main_exit",errname);    

#ifdef PARALLEL
  sim.talk.end_task();
#endif

  bob.message("done");

  return(0);
}

#endif
