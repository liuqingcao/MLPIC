#ifndef BOX_H
#define BOX_H

#include "common.h"
#include <fstream>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include "error.h"
#include "cell.h"
#include "particle.h"
#include "parameter.h"
#include "domain.h"
#include "diagnostic.h"
#include "uhr.h"
#include "readfile.h"

using namespace std;


class input_box {
private:
  char errname[filename_size];

public:
  int      Q_restart;           // start from t>0, using restart files
  char     restart_file[filename_size];
  int      Q_restart_save;
  char     restart_file_save[filename_size];

  int      n_domains;           
  int      Q_reorganize;
  int      delta_reo;

  int      nsp;

  readfile rf;
  void save( parameter &p );

  input_box( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class box {
private:
  char errname[filename_size];
  input_box input;

public:
  box( parameter &p );



  struct rest_struct {
  int Q_restart_save;
  int delta_rest;
  int count_rest;
  } rest;

  void    init_restart( parameter &p );
  void   count_restart( void );
  void    restart_save( diagnostic &diag, double time, parameter &p,
			uhr &zeit, uhr &zeit_particles, uhr &zeit_fields, 
			uhr &zeit_diagnostic);

  domain   grid;

  int n_domains;          // # of domains

  int n_el;               // total # of electrons
  int n_ion;              // total # of ions 
  int n_part;             // total # particles
};

#endif



