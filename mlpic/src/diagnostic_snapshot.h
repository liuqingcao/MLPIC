#ifndef DIAGNOSTIC_SNAPSHOT_H
#define DIAGNOSTIC_SNAPSHOT_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>
#include "readfile.h"

using namespace std;

class input_snapshot {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper; 
  int           Q_restart;
  char          restart_file[filename_size];

  input_snapshot( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class snapshot {
private:

  readfile       rf;
  input_snapshot input;
  char           errname[filename_size];

public:

  diagnostic_stepper stepper;

  char     *name;
  ofstream file;

  snapshot        ( parameter &p );
  void write_snap ( double time, domain* grid, parameter &p );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
