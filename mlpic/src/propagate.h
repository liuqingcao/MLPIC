#ifndef PROPAGATE_H
#define PROPAGATE_H

#include "common.h"
#include "error.h"
#include "parameter.h"
#include "pulse.h"
#include "box.h"
#include "particle.h"
#include "stack.h"
#include "diagnostic.h"
#include "uhr.h"
#include "readfile.h"

using namespace std;

class input_propagate {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  double start_time, stop_time;

  int    n_domains;

  int    Q_restart;
  char   restart_file[filename_size];

  input_propagate( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class propagate {
public:
                  propagate( parameter &p, domain &grid );
    void               loop( parameter &p, box &sim, 
			     pulse &laser_front, pulse &laser_rear,
			     diagnostic &diag );

private:
    input_propagate input;

    stack      stk;
    readfile   rf;
    double     time, start_time, stop_time;
    double     dt, dx, idx;                  // timestep and grid spacing
    double     Gamma;                        // gamma factor due to Lorentz Transformation
    int        domain_number;                // domain number
    int        n_domains;                    // # of domains

    ofstream grid_file;

    char errname[filename_size];
		  
    void                clear_grid( domain &grid );
    void                    fields( domain &grid, pulse &laser_front, pulse &laser_rear );
    void                 particles( domain &grid );
    void         reflect_particles( domain &grid );
    inline void	        accelerate( struct cell *cell, struct particle *part );
    inline void	      accelerate_1( struct cell *cell, struct particle *part );
    inline void	      accelerate_2( struct cell *cell, struct particle *part );
    inline void               move( struct particle *part );
    inline void has_to_change_cell( struct cell *cell, struct particle *part );
    inline void     do_change_cell( domain &grid );
    inline void     deposit_charge( struct cell *cell, struct particle *part );
    inline void    deposit_current( struct cell *cell, struct particle *part );
    inline void       mask_current( domain &grid );
    inline double             mask( int i );
    inline void           left_one( struct cell *cell, struct particle *part );
    inline void      left_two_left( struct cell *cell, struct particle *part );
    inline void     left_two_right( struct cell *cell, struct particle *part );
    inline void          right_one( struct cell *cell, struct particle *part );
    inline void    right_two_right( struct cell *cell, struct particle *part );
    inline void     right_two_left( struct cell *cell, struct particle *part );

    inline double        weighting( struct cell *cell, struct particle *part );
    inline double      weighting_0( struct cell *cell, struct particle *part );

};
#endif

