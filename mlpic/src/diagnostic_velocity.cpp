#include "diagnostic_velocity.h"


//////////////////////////////////////////////////////////////////////////////////////////

el_velocity::el_velocity( parameter &p,
			  int species_input, char *species_name_input )
  : velocity(p,species_input,species_name_input)
{
}

ion_velocity::ion_velocity( parameter &p,
			    int species_input, char *species_name_input )
  : velocity(p,species_input,species_name_input)
{
}

//////////////////////////////////////////////////////////////////////////////////////////


velocity::velocity( parameter &p,
		    int species_input, char *species_name_input )
  : rf(),
    input(p,species_name_input),
    stepper( input.stepper, p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("velocity::Constructor",errname);

  species = species_input;
  sprintf( species_name, "%s", species_name_input );

  dim        = 399;       // 400 velocity bins: 0...399

  x          = new int [ dim ];
  y          = new int [ dim ];
  z          = new int [ dim ];
  a          = new int [ dim ];

  Beta       = p.Beta;
  Gamma      = p.Gamma;

  vcut       = 1.0;

  name  = new( char [filename_size] );

}


//////////////////////////////////////////////////////////////////////////////////////////


input_velocity::input_velocity( parameter &p, char *species_name_input )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_velocity::Constructor",errname);
  char input_name[filename_size];

  sprintf( species_name, "%s", species_name_input );

  rf.openinput( p.input_file_name );

  sprintf( input_name, "&%s_velocity", species_name );
  stepper.Q         = atoi( rf.setget( input_name, "Q" ) );
  stepper.t_start   = atof( rf.setget( input_name, "t_start" ) );
  stepper.t_stop    = atof( rf.setget( input_name, "t_stop" ) );
  stepper.t_step    = atof( rf.setget( input_name, "t_step" ) );

  stepper.x_start   = atoi( rf.setget( input_name, "x_start" ) );
  stepper.x_stop    = atoi( rf.setget( input_name, "x_stop" ) );
  stepper.x_step    = -1; // not used

  Q_restart       = 0;
  if(stepper.Q == 1) system("mkdir -p ./data/velocity");

  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_velocity::save( parameter &p )
{
  static error_handler bob("input_velocity::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic " << species_name << "_velocity" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void velocity::write_velocity( double time, parameter &p, domain *grid )
{
  static error_handler bob("velocity::write_velocity",errname);

  struct cell *cell;
  struct particle *part;
  int i;
  double vx, vy, vz, v, absolut;
  int bvx, bvy, bvz, bv;


  for( i=0; i<=dim; i++ )
    x[i]=y[i]=z[i]=a[i]=0;

int nsp = p.nsp;

	for(int j=0;j<nsp;j++)
	{

  for( cell=grid->left; cell!=grid->rbuf; cell=cell->next ) {

      if (cell->npart != 0 && cell->number >= stepper.x_start
                           && cell->number < stepper.x_stop) {

	for( part=cell->first; part!=NULL; part=part->next ) {

	    if (part->species == species && part->target == j+1 ) {
	      vx = part->ux * part->igamma;
	      vy = part->uy * part->igamma;
	      vz = part->uz * part->igamma;

	      vx = 1.0/Gamma * vx / ( 1 + vy * Beta );
	      vz = 1.0/Gamma * vz / ( 1 + vy * Beta );
	      vy = ( vy + Beta ) / ( 1 + vy * Beta );
	      absolut = sqrt( sqr(vx) + sqr(vy) + sqr(vz) );

	      bvx = (int) floor( 0.5 * dim * (1.0 + vx/vcut) + 0.5 );
	      bvy = (int) floor( 0.5 * dim * (1.0 + vy/vcut) + 0.5 );
	      bvz = (int) floor( 0.5 * dim * (1.0 + vz/vcut) + 0.5 );
	      bv  = (int) floor( 0.5 * dim * (1.0 + absolut/vcut) + 0.5 );

	      if (bvx>=0 && bvx<=dim) x[bvx]++;
	      else bob.error( "velocity bin out of range" );
	      if (bvy>=0 && bvy<=dim) y[bvy]++;
	      else bob.error( "velocity bin out of range" );
	      if (bvz>=0 && bvz<=dim) z[bvz]++;
	      else bob.error( "velocity bin out of range" );
	      if (bv>=0 && bv<=dim)   a[bv]++;
	      else bob.error( "velocity bin out of range" );
	    }
	  }
      }
    }

  sprintf(name,"%s/velocity/velocity-%d-sp%d-%.3f", p.path, p.nsp, species, time);
  file.open(name);
   if (!file) bob.error("cannot open velocity file", name );

  file.precision( 16 );
  file.setf( ios::showpoint | ios::scientific ); 

  file<< setw(5) << "V"
      << setw(10) << "Vx_num"  
      << setw(10) << "Vy_num"     
      << setw(10) << "Vz_num"   
      << setw(10) << "V_num"<<endl;	

  for( i=0; i<=dim; i++ ) {
    v = -vcut + 2.0*vcut*i/dim;
    
    
    file<< v <<"   "
    << x[i] <<"   "
    << y[i] <<"   "
    << z[i] <<endl;
  }

  file.close();

}
}


//////////////////////////////////////////////////////////////////////////////////////////
//eof







