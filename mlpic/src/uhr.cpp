#include "uhr.h"

//////////////////////////////////////////////////////////////////////////////////////////

uhr::uhr( parameter &p, char *name )
  : rf(),
    input(p)
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("uhr::Constructor",errname);

  domain_number = p.domain_number;
  strcpy( uhrname, name ); 
  strcpy( path, p.path );

  reset();

}

//////////////////////////////////////////////////////////////////////////////////////////

input_uhr::input_uhr( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_uhr::Constructor",errname);

  rf.openinput( p.input_file_name );

  Q_restart         = 0;

  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_uhr::save( parameter &p )
{
  static error_handler bob("input_uhr::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "uhr" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
 
  outfile.close();

  bob.message("parameter written");
}

//////////////////////////////////////////////////////////////////////////////////////////

void uhr::start( void )
{
  static error_handler bob("uhr::start",errname);
  start_tics = clock();
}

//////////////////////////////////////////////////////////////////////////////////////////

void uhr::add( void )
{
  static error_handler bob("uhr::add",errname);
  stop_and_add();
  start_tics = clock();
}

//////////////////////////////////////////////////////////////////////////////////////////

void uhr::stop_and_add( void )
{
  static error_handler bob("uhr::stop_and_add",errname);
  double h, m, s;

  stop_tics = clock();

  if ( stop_tics - start_tics > 0 )
    {
      tics += ( stop_tics - start_tics );

      sec_cpu += (double)( stop_tics - start_tics ) / CLOCKS_PER_SEC;     

      m = 60.0*modf( (double)( stop_tics - start_tics ) / CLOCKS_PER_SEC/3600, &h );
      s = 60.0*modf( m, &m );

      s_cpu += s;
      if ( s_cpu >= 60 ) { s_cpu -= 60; m_cpu += 1; }

      m_cpu += m; 
      if ( m_cpu >= 60 ) { m_cpu -= 60; h_cpu += 1; } 

      h_cpu += h;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

void uhr::sys( void )
{
  static error_handler bob("uhr::sys",errname);
  time(&stop_time);

  sec_sys = difftime(stop_time,start_time);

  m_sys = 60*modf( difftime(stop_time,start_time)/3600, &(h_sys) );
  s_sys = 60*modf( m_sys, &(m_sys) );
}

//////////////////////////////////////////////////////////////////////////////////////////

void uhr::reset( void )
{
  static error_handler bob("uhr::reset",errname);
  start_tics = clock();

  start_time = time( &start_time );
  
  sec_cpu = sec_sys = 0;

  h_cpu = m_cpu = s_cpu = 0;
  h_sys = m_sys = s_sys = 0;

  tics = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////


void uhr::seconds_cpu( void )
{
  static error_handler bob("uhr::seconds_cpu",errname);
  ofstream f;
  char filename[filename_size];

  double seconds = (double) tics / CLOCKS_PER_SEC;

  cout << " cpu " << setw(7) << seconds << " sec : " << uhrname << " ";
  if ( fabs(seconds-sec_cpu)/sec_cpu > 1e-6 ) cout << sec_cpu;
  cout << endl;

  sprintf( filename, "%s/times", path );
  f.open(filename,ios::app);

  f << " cpu " << setw(7) << seconds << " sec : " << uhrname << " ";  
  if ( fabs(seconds-sec_cpu)/sec_cpu > 1e-6 ) f << sec_cpu; 
  f << endl;

  f.close();
}


//////////////////////////////////////////////////////////////////////////////////////////


void uhr::seconds_sys( void )
{
  static error_handler bob("uhr::seconds_sys",errname);
  ofstream f;
  char filename[filename_size];

  sys();

  cout << " sys " << setw(7) << sec_sys << " sec : " << uhrname << endl;

  sprintf( filename, "%s/times", path );
  f.open(filename,ios::app);

  f << " sys " << setw(7) << sec_sys << " sec : " << uhrname << endl;  

  f.close();
}








