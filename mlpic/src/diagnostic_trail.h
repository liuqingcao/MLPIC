#ifndef DIAGNOSTIC_TRAIL_H
#define DIAGNOSTIC_TRAIL_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include <cmath>
#include "readfile.h"

using namespace std;

class input_trail {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper;
  int           Q_restart;
  char          restart_file[filename_size];

  int           trails;
  int           *trailnum;

  input_trail( parameter &p, int species, char* input_species_name );
};


//////////////////////////////////////////////////////////////////////////////////////////


class trail {
private:

  readfile             rf;
  input_trail          input;

  int                  species;
  char                 *species_name;

   char      *errname;

public:

  diagnostic_stepper   stepper;

                            trail( parameter &p, int input_species, char *input_species_name );
  void                store_trail( double time, domain *grid, parameter &p );
  void               write_trail ( double time, struct particle *part, parameter &p );
};

class el_trail : public trail {
      public:
             el_trail(parameter &p, int input_species=0, char * input_species_name="el");
};

class ion_trail : public trail {
      public:
             ion_trail(parameter &p, int input_species=1, char * input_species_name="ion");
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
