#include "diagnostic_trace.h"


//////////////////////////////////////////////////////////////////////////////////////////


trace::trace( parameter &p )
  : rf(),
    input(p),
    stepper( input.stepper, p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("trace::Constructor",errname);

  int i, j;
  
  Gamma=p.Gamma;
  Beta=p.Beta;

  if(stepper.Q){
    name  = new( char [filename_size] );

    traces = input.traces;
    
    if (traces > 0) {
      cell_number = new( int [traces+1] );
      fp     = fmatrix( 1, traces, 0, p.spp - 1 );
      fm     = fmatrix( 1, traces, 0, p.spp - 1 );
      gp     = fmatrix( 1, traces, 0, p.spp - 1 );
      gm     = fmatrix( 1, traces, 0, p.spp - 1 );
      ex     = fmatrix( 1, traces, 0, p.spp - 1 );
      ey     = fmatrix( 1, traces, 0, p.spp - 1 );      
      ez     = fmatrix( 1, traces, 0, p.spp - 1 );
      by     = fmatrix( 1, traces, 0, p.spp - 1 );
      bz     = fmatrix( 1, traces, 0, p.spp - 1 );            
      dens_e = fmatrix( 1, traces, 0, p.spp - 1 );
      dens_i = fmatrix( 1, traces, 0, p.spp - 1 );
      jx     = fmatrix( 1, traces, 0, p.spp - 1 );
      jy     = fmatrix( 1, traces, 0, p.spp - 1 );
      jz     = fmatrix( 1, traces, 0, p.spp - 1 );

    }
    
    for( i=1; i<=traces; i++ ) {
      for( j=0; j<p.spp; j++ ) {
	fp[i][j]=0;
	fm[i][j]=0;
	gp[i][j]=0;
	gm[i][j]=0;
	ex[i][j]=0;
	ey[i][j]=0;
	ez[i][j]=0;
	by[i][j]=0;
	bz[i][j]=0;
	dens_e[i][j]=0;
	dens_i[i][j]=0;
	jx[i][j]=0;
	jy[i][j]=0;
	jz[i][j]=0;
   
      }
    }

    for( i=0, j=1; i<input.traces; i++, j++ ) cell_number[j] = input.tracepos[i];
  }


}


//////////////////////////////////////////////////////////////////////////////////////////


input_trace::input_trace( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_trace::Constructor",errname);
  int i;
  char name[filename_size];

  domain_number     = p.domain_number;

  rf.openinput( p.input_file_name );

  path = new char [ filename_size ];
  strcpy( path, rf.setget( "&output", "path" ) );

  stepper.Q         = atoi( rf.setget( "&traces", "Q" ) );
  stepper.t_start   = atof( rf.setget( "&propagate", "prop_start" ) );
  stepper.t_stop    = atof( rf.setget( "&propagate", "prop_stop" ) );
  stepper.t_step    = 1;         // write output once per period, but store each time step
  stepper.x_start   = 0;
  stepper.x_stop    = atoi( rf.setget( "&box", "cells" ) );
  stepper.x_step    = 0; 

  traces            = atoi( rf.setget( "&traces", "traces" ) );

  tracepos = new int [traces];
  for(i=0;i<traces;i++) {
    sprintf(name,"t%d",i);        // trace positions expected in variables t0, t1, t2, ...
     tracepos[i] =  atoi( rf.setget( "&traces", name ) );
  }

  Q_restart       = 0;

  rf.closeinput();
  if(stepper.Q == 1) system("mkdir -p ./data/trace");
  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_trace::save( parameter &p )
{
  static error_handler bob("input_trace::save",errname);
  ofstream outfile;
  int i;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic trace" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "traces           : " << traces          << endl;
  outfile << "                 : "; 
  for(i=0;i<traces;i++) outfile << tracepos[i] << " ";       
  outfile << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void trace::store_traces( domain* grid )
{
  static error_handler bob("trace::store_traces",errname);
  struct cell *cell;
  int i;

  for( cell=grid->left; cell->next!=grid->rbuf; cell=cell->next )
    {
      for( i=1; i<=traces; i++ ) {
	
	if ( cell_number[i]==cell->number ) {
    
	  fp[i][stepper.t_count]     = (float) cell->fp;
	  fm[i][stepper.t_count]     = (float) cell->fm;
	  gp[i][stepper.t_count]     = (float) cell->gp;
	  gm[i][stepper.t_count]     = (float) cell->gm;
	  
	  ey[i][stepper.t_count]     = (float)( cell->fp + cell->fm);
	  bz[i][stepper.t_count]     = (float)( cell->fp - cell->fm);	  
	  by[i][stepper.t_count]     = (float)( cell->gp - cell->gm);	  
	  ez[i][stepper.t_count]     = (float)( cell->gp + cell->gm);
	  	  
	  ex[i][stepper.t_count]     = (float) cell->ex;
	  dens_e[i][stepper.t_count] = (float) cell->dens[0];
	  dens_i[i][stepper.t_count] = (float) cell->dens[1];
	  jx[i][stepper.t_count]     = (float) cell->jx;
	  jy[i][stepper.t_count]     = (float) cell->jy;
	  jz[i][stepper.t_count]     = (float) cell->jz;
	  
	}
      }
    }    
}


//////////////////////////////////////////////////////////////////////////////////////////

void trace::write_traces( double time, parameter &p )
{
  static error_handler bob("trace::write_traces",errname);

  int   period = (int) floor( time + 0.5 );
  float position;
  int   i, j;


  for( i=1; i<=traces; i++ ) {

  sprintf(name,"%s/trace/trace-%d", p.path, cell_number[i]);
  
  file.open( name,ios::app);
  if (!file) bob.error("cannot open trace file", name );

  file.precision( 16 );
  file.setf( ios::showpoint | ios::scientific );

  // main body of the trace file
	if(period == 1)
	{
	  file<< setw(5) << "time"
		  << setw(10) << "ex"  
		  << setw(10) << "ey"     
		  << setw(10) << "ez"      
		  << setw(10) << "by"     
		  << setw(10) << "bz"      
		  << setw(15) << "dens_e" 
		  << setw(15) << "dens_i" 
		  << setw(10) << "jx" 
		  << setw(10) << "jy"
		  << setw(10) << "jz" << endl;
	}

	for(int j=0;j<stepper.t_step;j++)
	{
		double time_now = time-1.0 + (double)j/(double)stepper.t_step;
		file<<time_now<<"   "
			<<ex[i][j]<<"   "
			<<ey[i][j]<<"   "
			<<ez[i][j]<<"   "
			<<by[i][j]<<"   "
			<<bz[i][j]<<"   "
			<<dens_e[i][j]<<"   "
			<<dens_i[i][j]<<"   "
			<<jx[i][j]<<"   "
			<<jy[i][j]<<"   "
			<<jz[i][j]<<endl;
	}
	file.close();
  }



  for( i=1; i<=traces; i++ ) {
    for( j=0; j<p.spp; j++ ) {
      fp[i][j]=0;
      fm[i][j]=0;
      gp[i][j]=0;
      gm[i][j]=0;
      ex[i][j]=0;
      ey[i][j]=0;
      ez[i][j]=0;
      by[i][j]=0;
      bz[i][j]=0;      
      dens_e[i][j]=0;
      dens_i[i][j]=0;
      jx[i][j]=0;
      jy[i][j]=0;
      jz[i][j]=0;

    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////


void trace::restart_save( void )
{
  static error_handler bob("trace::restart_save",errname);
  ofstream file1;
  char fname[ filename_size ];
  char  dataname[filename_size];
  int i;
      
  sprintf( fname, "%s/%s-%d-data1", input.path, input.restart_file_save, 
	   input.domain_number);
  file1.open(fname,ios::app);
  if (!file1) bob.error( "cannot open file", fname );
      
  file1.precision( 20 );
  file1.setf( ios::showpoint | ios::scientific );

  for(i=1; i<=traces; i++){

    sprintf( dataname, "fp[%d][0]     = ", i);
    file1 << dataname <<  fp[i][0] << endl;
    sprintf( dataname, "fm[%d][0]     = ", i);
    file1 << dataname <<  fm[i][0] << endl;
    sprintf( dataname, "gp[%d][0]     = ", i);
    file1 << dataname <<  gp[i][0] << endl;
    sprintf( dataname, "gm[%d][0]     = ", i);
    file1 << dataname <<  gm[i][0] << endl;
    sprintf( dataname, "ex[%d][0]     = ", i);
    file1 << dataname <<  ex[i][0] << endl;
    sprintf( dataname, "dens_e[%d][0] = ", i);
    file1 << dataname <<  dens_e[i][0] << endl;
    sprintf( dataname, "dens_i[%d][0] = ", i);
    file1 << dataname <<  dens_i[i][0] << endl;
    sprintf( dataname, "jx[%d][0]     = ", i);
    file1 << dataname <<  jx[i][0] << endl;
    sprintf( dataname, "jy[%d][0]     = ", i);
    file1 << dataname <<  jy[i][0] << endl;
    sprintf( dataname, "jz[%d][0]     = ", i);
    file1 << dataname <<  jz[i][0] << endl;
  }  
  file1.close();
}

//////////////////////////////////////////////////////////////////////////////////////////
float trace::average_vx(struct cell* cell){
      float avx=0,vx,vy;
      struct particle* part;
      if (cell->np[0]!= 0) {
          for( part=cell->first; part!=NULL; part=part->next ) {
              if (part->species == 0) {
	                      vx = part->ux * part->igamma;
	                      vy = part->uy * part->igamma;
	                      //vz = part->uz * part->igamma;

	                      vx = 1.0/Gamma * vx / ( 1 + vy * Beta );
	                      //vz = 1.0/Gamma * vz / ( 1 + vy * Beta );
	                      //vy = ( vy + Beta ) / ( 1 + vy * Beta );
	                      avx+=vx;
                       }
              }
          avx=avx/cell->np[0];
          }

      return avx;
}

float trace::average_vy(struct cell* cell){
      float avy=0,vy;
      struct particle* part;
      if (cell->np[0] != 0) {
          for( part=cell->first; part!=NULL; part=part->next ) {
              if (part->species == 0) {
	                      //vx = part->ux * part->igamma;
	                      vy = part->uy * part->igamma;
	                      //vz = part->uz * part->igamma;

	                      //vx = 1.0/Gamma * vx / ( 1 + vy * Beta );
	                      //vz = 1.0/Gamma * vz / ( 1 + vy * Beta );
	                      vy = ( vy + Beta ) / ( 1 + vy * Beta );
	                      avy+=vy;
                       }
              }
          avy=avy/cell->np[0];
          }

      return avy;
}

float trace::average_vz(struct cell* cell){
      float avz=0,vy,vz;
      struct particle* part;
      if (cell->np[0] != 0) {
          for( part=cell->first; part!=NULL; part=part->next ) {
              if (part->species == 0) {
	                      //vx = part->ux * part->igamma;
	                      vy = part->uy * part->igamma;
	                      vz = part->uz * part->igamma;

	                      //vx = 1.0/Gamma * vx / ( 1 + vy * Beta );
	                      vz = 1.0/Gamma * vz / ( 1 + vy * Beta );
	                      //vy = ( vy + Beta ) / ( 1 + vy * Beta );
	                      avz+=vz;
                       }
              }
          avz=avz/cell->np[0];
          }

      return avz;
}
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//eof

