#include "diagnostic_trail.h"

//////////////////////////////////////////////////////////////////////////////////////////
el_trail::el_trail(parameter &p, int input_species, char *input_species_name)
         :trail(p, input_species, input_species_name)
{
}

ion_trail::ion_trail(parameter &p, int input_species, char *input_species_name)
         :trail(p, input_species, input_species_name)
{
}
//////////////////////////////////////////////////////////////////////////////////////////


trail::trail( parameter &p,int input_species, char *input_species_name )
  : rf(),
    input(p,input_species,input_species_name),
    stepper( input.stepper, p )
{

  char *fname;
  errname         = new char [filename_size] ;
  fname           = new char [filename_size];
  species_name    = new char [filename_size];


  sprintf( errname, "%s/error", p.path);
  static error_handler shasha("trail::Constructor",errname);
  species=input_species;
  sprintf(species_name,"%s",input_species_name);

}


//////////////////////////////////////////////////////////////////////////////////////////


input_trail::input_trail( parameter &p, int input_species,char * input_species_name )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler shasha("input_trail::Constructor",errname);
  ofstream file;

  char       *name;
  char       *fname;
  char       *keyname;

  name         = new char [filename_size] ;
  fname        = new char [filename_size] ;
  keyname      = new char [filename_size] ;



  sprintf(keyname,"&%s_trail",input_species_name);

  rf.openinput( p.input_file_name );

  stepper.Q         = atoi( rf.setget( keyname, "Q" ) );
  stepper.t_start   = atof( rf.setget( keyname, "t_start" ) );
  stepper.t_stop    = atof( rf.setget( keyname, "t_stop" ) );
  stepper.t_step    = atof( rf.setget( keyname, "t_step" ) );
  stepper.x_start   = -1;   // not used
  stepper.x_stop    = -1;   // not used
  stepper.x_step    = -1;   // not used
  if(stepper.Q == 1) system("mkdir -p ./data/trail");
  if(1/stepper.t_step>p.spp) shasha.error("Too small step in the trail diagnostic!");

  trails            = atoi( rf.setget( keyname, "trails" ) );

  trailnum = new int [1000];

  int narg,count;
  int n=0,x;
  char **arg;
  arg=new char*[50];

  for(int i=0;i<50;i++)arg[i]=new char[100];

  rf.setvar(keyname,"t_num");

  do{
      count=0;
      rf.read_line(&narg,arg);
      for( int i=0;i<narg;i++)
      {
          x=atoi(arg[i]);
          if(x!=0)
          {
              trailnum[n]=x;
              n++;
              count++;
          }
      }
  }while(count!=0);

  if(trails!=n) shasha.error("The trails does not match with the numbers.");

  if(stepper.Q == 1)
  {
    for(int i=0; i<trails;i++) {
      sprintf(fname,"%s/trail/trail-sp%d-%d",p.path,input_species,trailnum[i]);
      file.open(fname);
	file<< setw(5) << "time"
		 << setw(10) << "x" 
		  << setw(10) << "Vx"  
		  << setw(10) << "Vy"     
		  << setw(10) << "Vz"      
		  << setw(10) << "V"  
		  << setw(10) << "Gama_x"
		  << setw(10) << "Gama_y"		  
		  << setw(10) << "Gama_z"		  
		  << setw(10) << "Gama"<< endl;		        
		        
      if(!file) shasha.error("cannot open trail file",fname);
      file.close();
    }
  }
  Q_restart      = 0;

  rf.closeinput();

  shasha.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_trail::save( parameter &p )
{
  static error_handler shasha("input_trail::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic trail" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  shasha.message("parameter written----");
}

//////////////////////////////////////////////////////////////////////////////////////////


void trail::store_trail( double time, domain *grid, parameter &p )
{
  static error_handler shasha("trail::store_trail",errname);
  struct cell *cell;
  struct particle *part;

  for( cell = grid->left; cell != grid->rbuf; cell = cell->next )
  {
     if( cell->np[species] != 0 )      // Important!!
     {
        for( part = cell->first; part != NULL; part = part->next )
        {
           for( int i= 0; i < input.trails; i++ )
           {
              if( part->number == input.trailnum[i] && part->species == species )
                 write_trail( time, part, p );
           }
        }
     }
  }

}


//////////////////////////////////////////////////////////////////////////////////////////


void trail::write_trail( double time, struct particle *part, parameter &p )
{
  static error_handler shasha("trail::write_trail",errname);
  struct cell *cell;
  ofstream   file;
  double vx, vy, vz,v,gamx,gamy,gamz,gam;
  double x;

  x = part->x;

  vx = part->ux * part->igamma;
  vy = part->uy * part->igamma;
  vz = part->uz * part->igamma;

  vx = 1.0/p.Gamma * vx / ( 1 + vy * p.Beta );
  vz = 1.0/p.Gamma * vz / ( 1 + vy * p.Beta );
  vy = ( vy + p.Beta ) / ( 1 + vy * p.Beta );

	v = sqrt(vx*vx+vy*vy+vz*vz);
	gam = 1.0/sqrt(1.0-v*v);
	gamx = 1.0/sqrt(1.0-vx*vx);
	gamy = 1.0/sqrt(1.0-vy*vy);
	gamz = 1.0/sqrt(1.0-vz*vz);


  char *fname;

  fname         = new char [filename_size] ;


  sprintf( fname, "%s/trail/trail-sp%d-%d", p.path,species,part->number );

  file.open( fname,ios::app);
  if( !file ) shasha.error( "cannot open trail file", fname );
  
  file<<time<<"   "
		<<x<<"   "
  		<<vx<<"   "
		<<vy<<"   "
		<<vz<<"   "
		<<v<<"   "
		<<gamx<<"   "
  		<<gamy<<"   "
		<<gamz<<"   "
		<<gam<<endl;

  file.close();


}


//////////////////////////////////////////////////////////////////////////////////////////
//eof

