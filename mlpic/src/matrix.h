#ifndef MATRIX_H
#define MATRIX_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <iostream>
using namespace std;

float**          fmatrix( long nrl, long nrh, long ncl, long nch );
void      delete_fmatrix( float **m, long nrl, long nrh, long ncl, long nch );
double**         dmatrix( long nrl, long nrh, long ncl, long nch );
void      delete_dmatrix( double **m, long nrl, long nrh, long ncl, long nch );
unsigned char** ucmatrix( long nrl, long nrh, long ncl, long nch );
void     delete_ucmatrix( unsigned char **m, long nrl, long nrh, long ncl, long nch );
int**            imatrix( long nrl, long nrh, long ncl, long nch );
void      delete_imatrix( int **m, long nrl, long nrh, long ncl, long nch );

class Trash {
  friend ifstream& operator>> (ifstream&, Trash&);
private:
  char string[1];
};

void error(char* s1, char*  s2="", char* s3="", char*  s4="");
void error(char* s1, double d2,    char* s3="", char*  s4="");


#endif
