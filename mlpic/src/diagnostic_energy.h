#ifndef DIAGNOSTIC_ENERGY_H
#define DIAGNOSTIC_ENERGY_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;

class input_energy {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper; 
  int           Q_restart;
  char          restart_file[filename_size];

  input_energy( parameter &p );
};

//////////////////////////////////////////////////////////////////////////////////////////

class energy {
private:
  char         errname[filename_size];
  readfile     rf;
  input_energy input;

public:
  diagnostic_stepper stepper;

  double flux;
  double field, field_0;
  double field_t, field_t_0;
  double field_l, field_l_0;
  double kinetic, kinetic_0;
  double total, total_0;
  char *name;
  ofstream file;

  energy              ( parameter &p, domain* grid );
  void get_energies   ( domain* grid );
  void write_energies ( double time );
  void average_reflex ( domain *grid );
};  

//////////////////////////////////////////////////////////////////////////////////////////

#endif
