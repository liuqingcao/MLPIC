//////////////////////////////////////////////////////////////////////////////////////////
//
// header file, input parameters to lpi.C
//
// last change: June 8 1997
//
//////////////////////////////////////////////////////////////////////////////////////////

#ifndef PARAMETER_H
#define PARAMETER_H

#include "common.h"
#include "error.h"
#include <fstream>
#include <iomanip>
#include <cstring>
#include "matrix.h"
#include <cmath>
#include "readfile.h"
#include "stdlib.h"
using namespace std;
class parameter  {

private:
  void      adjust_angle_write_steps( void );
  void      save( void );
  readfile  rf;
  int       Q_restart;

public:

  char      *my_name;                // command line input
  char      *input_file_name;        // command line input or default value
  int       domain_number;           // command line input or default value
  int       n_domains;               // namelist input
  char      *path;                   // namelist input
  char      *errname;                // file name for output of errors and comments
  char      *outname;                // file name for output of input

  int       nsp;                     // initial number of particle species
  int       spl;                     // number of cells per laser wave length
  int       spp;                     // number of time steps per period
  double    angle, Beta, Gamma;      // Lorentz transformation

  parameter(int argc, char **argv);

};

//////////////////////////////////////////////////////////////////////////////////////////
#endif





