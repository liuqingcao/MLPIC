#include "parameter.h"

parameter::parameter(int argc, char **argv)
  : rf()
{
  my_name         = new char [filename_size];
  input_file_name = new char [filename_size] ;
  errname         = new char [filename_size] ;
  outname         = new char [filename_size] ;
  path            = new char [filename_size] ;

  strcpy(my_name,argv[0]);




  //////////////// check commandline parameters //////////////////////////////////////////

  if (argc==1) {           // no argument: assume domain=1 and input from input/input.list
    domain_number = 1;
    sprintf( input_file_name, "input.lpic" );
    system(" mkdir -p data ");
	system(" rm -rf ./data/* ");
	system(" rm -f ./data/* ");
  }
  else{           // one argument: interprete as domain number or input-file
    cout << "\n wrong: ";//               choose default for the other one
	 exit(-1);
  }


  //// read path and number of domains ///////////////////////////////////////////////////

  rf.openinput(input_file_name);

  strcpy( path, rf.setget( "&output", "path" ) );
  n_domains = 1;
  Q_restart = 0;

  cout << " input file  : " << input_file_name << endl;
  cout << " output path : " << path << endl << endl;

  //// set initial number of particle species ////////////////////////////////////////////
  nsp         = atoi( rf.setget( "&box", "species_number" ) );
  
rf.closeinput(); 


if(nsp <0 )
{
	cout<<"The 'species_number' can not be negetive!"<<endl;
	exit(-1);
}


if(nsp > 3 )
{
	cout<<"The 'species_number' is larger than 3!"<<endl;
	cout<<"You have to modify the array demension in 'cell.h' and restall the program. "<<endl;
	exit(-1);
}



  //// set in-out file name //////////////////////////////////////////////////////////////

  sprintf( outname, "%s/output.lpi", path );

  //// start first error handler /////////////////////////////////////////////////////////

  sprintf( errname, "%s/error", path );
  static error_handler bob("parameter::Constructor", errname);

#ifdef DEBUG
  bob.message("DEBUG is defined");
#else
  bob.message("DEBUG is undefined");
#endif

  bob.message( "program            =", my_name );
  bob.message( "domain number      =", domain_number );
  bob.message( "# domains          =", n_domains );
  bob.message( "# species          =", nsp );

  //// adjust angle such that # of steps per period is integer ///////////////////////////
  //// write spp and spl to file 'lpic.steps' for later use in lpic's postprocessor //////

  adjust_angle_write_steps();

  //// check contents of output directory and write in-out file for the first time ///////

  if (domain_number==1) {

    char input_copy[filename_size];
    sprintf( input_copy, "%s/input.lpi", path ); // write copy of input to path/input.lpi

    rf.copy_file( input_file_name, input_copy );
    
    }

    save();                                      // save parameters to path/output.lpi
  }



//////////////////////////////////////////////////////////////////////////////////////////


void parameter::adjust_angle_write_steps( void )
// read and adjust angle of incidence, calculate steps per period
// write spp and spl to file 'lpic.steps' for later use in lpic's postprocessor
{
  static error_handler bob("parameter::adjust_angle",errname);

  rf.openinput( input_file_name );

  int    front_Q1      = atoi( rf.setget( "&pulse_front", "Q_1" ) );
  int    rear_Q1       = atoi( rf.setget( "&pulse_rear", "Q_1"  ) );
  int    front_Q2      = atoi( rf.setget( "&pulse_front", "Q_2" ) );
  int    rear_Q2       = atoi( rf.setget( "&pulse_rear", "Q_2"  ) );
  double front_angle  = atof( rf.setget( "&pulse_front", "angle" ) );
  double rear_angle   = atof( rf.setget( "&pulse_rear", "angle" ) );

  if ( front_Q1 > 0||front_Q2>0 ) rear_angle = front_angle;
  else {
    if ( rear_Q1 > 0||rear_Q2>0 ) front_angle = rear_angle;
    else              rear_angle  = front_angle;
  }

  spl         = atoi( rf.setget( "&box", "cells_per_wl" ) );
  spp         = (int) floor( 1.0/cos(PI/180*front_angle) * spl + 0.5 );
  angle       = 180.0/PI * acos( (double) spl / spp );
  Beta        = sin( PI/180 * angle );
  Gamma       = 1.0 / cos( PI/180 * angle );

  bob.message( "angles adjusted to ", angle, "degree" );
  bob.message( "# spl             =", spl );
  bob.message( "# spp             =", spp );

  rf.closeinput();
  
  FILE *f;
  char fname[filename_size];
  sprintf( fname, "%s/lpic.steps", path );

  f = fopen( fname, "w" );
  fprintf( f, "spl = %d\n", spl );
  fprintf( f, "spp = %d\n", spp );
  fclose( f );
}


//////////////////////////////////////////////////////////////////////////////////////////


void parameter::save( void )
{
  static error_handler bob("parameter::save",errname);

  ofstream outfile(outname);
  if (!outfile) bob.error("Cannot open outfile: ", outname);

  outfile << "parameter" << endl;
  outfile << "------------------------------------------------------------------" << endl;

  outfile << "program name       : " << my_name         << endl;
  outfile << "input file         : " << input_file_name << endl;
  outfile << "output path        : " << path            << endl;
  outfile << "domain number      : " << domain_number   << endl;
  outfile << "# domains          : " << n_domains       << endl;
  outfile << "# species          : " << nsp             << endl;
  outfile << "# steps per cycle  : " << spp             << endl;
  outfile << "adjusted angle     : " << angle           << endl;
  outfile << "LT-Beta            : " << Beta            << endl;
  outfile << "LT-Gamma           : " << Gamma           << endl << endl << endl;

  outfile.close();
};

//////////////////////////////////////////////////////////////////////////////////////////
//EOF

