#include "pulse.h"

int pulse::pulse_number = 0;

pulse::pulse( parameter &p, char *side )
  : input(p,side,pulse_number+1)
{
  pulse_number ++;

  sprintf( errname, "%s/error", p.path);
  static error_handler bob("pulse::Constructor",errname);

  path = new char [filename_size];
  strcpy(path,p.path);

  if (input.Q1>0) {                  // pulse switched ON

    if (input.polarization1==1) { Qz1 = 1.0; Qy1 = 0.0; shift1 = 0.0; }
    if (input.polarization1==2) { Qz1 = 0.0; Qy1 = 1.0; shift1 = 0.0; }
    if (input.polarization1==3) { Qz1 = 1.0; Qy1 = 1.0; shift1 = 0.25; }
    if (input.polarization1==4) { Qz1 = 1.0; Qy1 = 1.0; shift1 = -0.25; }

    bob.message("pulse #", pulse_number, "generated");
    input.shape1 == 1 ? bob.message("shape       linear"):
    input.shape1 == 2 ? bob.message("shape       sin"):
    bob.message("shape       sinsqr");

    bob.message("amplitude    ", input.a01 );
    bob.message("raise_time   ", input.raise1, " periods");
    bob.message("duration     ", input.duration1, " periods");
    bob.message("polarization ", input.polarization1 );
    bob.message("color        ", input.a21, input.a31 );
    bob.message("phase        ", input.p21, input.p31 );

    if (input.Q_save==1 && p.domain_number==1 && input.Q_restart == 0)
      {
	save(input.time_start, input.duration1, input.save_step);
      }
  }
  else {                              // pulse switched OFF
    Qz1 = 0.0; Qy1 = 0.0; shift1 = 0.0;

    bob.message("pulse #", pulse_number, "is switched off");
  }
  if (input.Q2>0) {                  // pulse switched ON

    if (input.polarization2==1) { Qz2 = 1.0; Qy2 = 0.0; shift2 = 0.0; }
    if (input.polarization2==2) { Qz2 = 0.0; Qy2 = 1.0; shift2 = 0.0; }
    if (input.polarization2==3) { Qz2 = 1.0; Qy2 = 1.0; shift2 = 0.25/input.omega2; }
    if (input.polarization2==4) { Qz2 = 1.0; Qy2 = 1.0; shift2 = -0.25/input.omega2; }

    bob.message("pulse #", pulse_number, "generated");
    input.shape2 == 1 ? bob.message("shape       linear"):
    input.shape2 == 2 ? bob.message("shape       sin"):
    bob.message("shape       sinsqr");

    bob.message("amplitude    ", input.a02 );
    bob.message("raise_time   ", input.raise2, " periods");
    bob.message("duration     ", input.duration2, " periods");
    bob.message("polarization ", input.polarization2 );
    bob.message("color        ", input.a22, input.a32 );
    bob.message("phase        ", input.p22, input.p32 );

    if (input.Q_save==1 && p.domain_number==1 && input.Q_restart == 0)
      {
	save(input.time_start+input.delaytime, input.time_start+input.delaytime+ input.duration2, input.save_step);
      }
  }
  else {                              // pulse switched OFF
    Qz2 = 0.0; Qy2 = 0.0; shift2 = 0.0;

    bob.message("pulse #", pulse_number, "is switched off");
  }
}


//////////////////////////////////////////////////////////////////////////////////////////


input_pulse::input_pulse( parameter &p, char *side, int pulse_number )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_pulse::Constructor",errname);

  rf.openinput(p.input_file_name);

  Q1               = atoi( rf.setget( side, "Q_1"              ) );
  a01              = atof( rf.setget( side, "amplitude_1"      ) );
  p11              = atof( rf.setget( side, "phase1_1"         ) );
  c1               = atof( rf.setget( side, "chirp_1"          ) );
  a21              = atof( rf.setget( side, "amplitude2_1"     ) );
  a31              = atof( rf.setget( side, "amplitude3_1"     ) );
  p21              = atof( rf.setget( side, "phase2_1"         ) );
  p31              = atof( rf.setget( side, "phase3_1"         ) );
  polarization1    = atoi( rf.setget( side, "polarization_1"   ) );
  shape1           = atoi( rf.setget( side, "shape_1"          ) );
  raise1           = atof( rf.setget( side, "raise_1"          ) );
  duration1        = atof( rf.setget( side, "duration_1"       ) );
  
  Q2               = atoi( rf.setget( side, "Q_2"              ) );
  a02              = atof( rf.setget( side, "amplitude_2"      ) );
  c2               = atof( rf.setget( side, "chirp_2"          ) );
  a22              = atof( rf.setget( side, "amplitude2_2"     ) );
  a32              = atof( rf.setget( side, "amplitude3_2"     ) );
  p22              = atof( rf.setget( side, "phase2_2"         ) );
  p32              = atof( rf.setget( side, "phase3_2"         ) );
  polarization2    = atoi( rf.setget( side, "polarization_2"   ) );
  shape2           = atoi( rf.setget( side, "shape_2"          ) );
  raise2           = atof( rf.setget( side, "raise_2"          ) );
  duration2        = atof( rf.setget( side, "duration_2"       ) );

  omega2           = atof( rf.setget( side, "omega_2"          ) );

  delaytime        = atof( rf.setget( side, "delaytime"       ) );
  delayphase       = atof( rf.setget( side, "delayphase"      ) );

  Q_save           = atoi( rf.setget( side, "pulse_save"     ) );
  save_step        = atof( rf.setget( side, "pulse_save_step") );

  time_start      = atof( rf.setget( "&propagate", "prop_start") );
  time_stop       = atof( rf.setget( "&propagate", "prop_stop") );

  Q_restart       = 0;

  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p,pulse_number);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_pulse::save( parameter &p, int pulse_number )
{
  static error_handler bob("input_pulse::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "pulse # " << pulse_number << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                  : " << Q1              << endl;
  outfile << "a0                 : " << a01             << endl;
  outfile << "a2                 : " << a21             << endl;
  outfile << "a3                 : " << a31             << endl;
  outfile << "phase1             : " << p11             << endl;  
  outfile << "phase2             : " << p21             << endl;
  outfile << "phase3             : " << p31             << endl;
  outfile << "polarization       : " << polarization1   << endl;
  outfile << "shape              : " << shape1          << endl;
  outfile << "raise              : " << raise1          << endl;
  outfile << "duration           : " << duration1       << endl;
  outfile << "Q_save             : " << Q_save         << endl;
  outfile << "save_step          : " << save_step      << endl;
  outfile << "time_start         : " << time_start     << endl;
  outfile << "time_stop          : " << time_stop      << endl;
  outfile << "Q_restart          : " << Q_restart      << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void pulse::save(double t_start, double t_stop, double t_step)
{
    static error_handler bob("pulse::save",errname);
    char filename[filename_size];

    if (t_stop < t_start)
	bob.error("stop_time smaller than start_time");
    if (t_start < 0.)
	bob.error("start_time smaller than 0 not allowed for current pulse");
    if (t_step <0.)
	bob.error("step is negative !");

    sprintf(filename,"%s/pulse#%d", path, pulse_number );

    ofstream pulse_file(filename,ios::app | ios::ate);
    if (!pulse_file)
	bob.error("cannot open output file: ", filename);

    pulse_file.precision( 4 );
    pulse_file.setf( ios::scientific | ios::showpoint );

    for (double t=t_start; t<t_stop; t+=t_step)
	pulse_file << setw(15) << t << " " << setw(15) << fieldey(t)<<setw(15) << fieldez(t)<<setw(15) << sqrt(sqr(fieldey(t)) + sqr(fieldez(t))) << endl;

    pulse_file.close();
}


//////////////////////////////////////////////////////////////////////////////////////////

double pulse::envelope1( double t )
{
  return (
  	  input.shape1 == 1 ?
	  (
	   t < input.raise1 ?            ( t / input.raise1 ) :
	   t < input.duration1 - input.raise1 ? 1.0 :
	   t < input.duration1 ?         ( input.duration1 - t ) / input.raise1 :
	   0
	   ) :
	   input.shape1 == 2 ?
	   (
	    t < input.raise1 ? 	   sin( 0.5 * PI * t / input.raise1 ) :
	    t < input.duration1 - input.raise1 ? 1.0 :
	    t < input.duration1 ?         sin( 0.5 * PI * (input.duration1 - t) / input.raise1 ) :
	    0
	    ) :
	   input.shape1 == 3 ?
           (
	     t < input.raise1 ?         sqr( sin( 0.5 * PI * t / input.raise1 ) ) :
	     t < input.duration1 - input.raise1 ? 1.0 :
	     t < input.duration1 ?      sqr( sin( 0.5 * PI * (input.duration1 - t) / input.raise1 ) ) :
	     0
	     ) :
	   input.shape1 == 4 ?
           (
	     t < input.raise1 ?         sqrt( sin( 0.5 * PI * t / input.raise1 ) ) :
	     t < input.duration1 - input.raise1 ? 1.0 :
	     t < input.duration1 ?      sqrt( sin( 0.5 * PI * (input.duration1 - t) / input.raise1 ) ) :
	     0
	     ) :	     
	   input.shape1 == 5 ?
           (
	     t < input.raise1 ?         sqr( sin( 0.5 * PI * t / 10.0 ) ) :
	       0
	     ) :
			0

 	     );
}

//////////////////////////////////////////////////////////////////////////////////////////
double pulse::envelope2( double t )
{
  t-=input.delaytime;
  if(t<=0) return 0;
  else {
  return (
	  input.shape2 == 1 ?
	  (
	   t < input.raise2 ?            ( t / input.raise2 ) :
	   t < input.duration2 - input.raise2 ? 1.0 :
	   t < input.duration2 ?         ( input.duration2 - t ) / input.raise2 :
	   0
	   ) :
	   input.shape2 == 2 ?
	   (
	    t < input.raise2 ? 	   sin( 0.5 * PI * t / input.raise2 ) :
	    t < input.duration2 - input.raise2 ? 1.0 :
	    t < input.duration2 ?         sin( 0.5 * PI * (input.duration2 - t) / input.raise2 ) :
	    0
	    ) :
	   input.shape2 == 3 ?
       ( t < input.raise2 ?         sqr( sin( 0.5 * PI * t / input.raise2 ) ) :
	     t < input.duration2 - input.raise2 ? 1.0 :
	     t < input.duration2 ?      sqr( sin( 0.5 * PI * (input.duration2 - t) / input.raise2 ) ) :
	     0
	     ) :
	   input.shape1 == 4 ?
           (
	     t < input.raise2 ?         ( sin( 0.5 * PI * t / input.raise2 ) ) * ( sin( 0.5 * PI * t / input.raise2 ) ) :
	     t < input.duration2 - input.raise2 ? 1.0 :
	     t < input.duration2 ?      ( sin( 0.5 * PI * (input.duration2 - t) / input.raise2 ) )*( sin( 0.5 * PI * (input.duration2 - t) / input.raise2 ) ) :
	     0
	     ) :
	     0
   );
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

double pulse::fieldey( double t )
{
  double env1 = envelope1( t );
  double env2 = envelope2( t );
  double amp;

  amp =  Qy1*input.a01 * env1 * sin( 2.0*PI*(t+0.5*input.duration1*input.c1*(t/input.duration1-0.5)*(t/input.duration1-0.5)) +input.p11 );
  amp += Qy2*input.a02 * env2 * sin( 2.0*PI*input.omega2*(t+0.5*input.duration2*input.c2*(t/input.duration2-0.5)*(t/input.duration2-0.5))+input.delayphase );

  amp += Qy1*input.a21 * env1 * sin( 4.0*PI*t + input.p21 );
  amp += Qy2*input.a22 * env2 * sin( 4.0*PI*input.omega2*t + input.p22 + input.delayphase );

  amp += Qy1*input.a31 * env1 * sin( 6.0*PI*t + input.p31 );
  amp += Qy2*input.a32 * env2 * sin( 6.0*PI*input.omega2*t + input.p32 + input.delayphase );


  return amp;
}

//////////////////////////////////////////////////////////////////////////////////////////
double pulse::fieldez( double t )
{

  double env1 = envelope1( t );
  double env2 = envelope2( t );
  double amp;
  double t1=t+shift1;
  double t2=t+shift2;

  amp =  Qz1*input.a01 * env1 * sin( 2.0*PI*(t1+0.5*input.duration1*input.c1*(t1/input.duration1-0.5)*(t1/input.duration1-0.5)) +input.p11 );
  amp += Qz2*input.a02 * env2 * sin( 2.0*PI*input.omega2*(t2+0.5*input.duration2*input.c2*(t2/input.duration2-0.5)*(t2/input.duration2-0.5))+input.delayphase );
 
  amp += Qz1*input.a21 * env1 * sin( 4.0*PI*t1 + input.p21 );
  amp += Qz2*input.a22 * env2 * sin( 4.0*PI*input.omega2*t2 + input.p22 + input.delayphase );
 
  amp += Qz1*input.a31 * env1 * sin( 6.0*PI*t1 + input.p31 );
  amp += Qz2*input.a32 * env2 * sin( 6.0*PI*input.omega2*t2 + input.p32 + input.delayphase );


  return amp;
}

//////////////////////////////////////////////////////////////////////////////////////////
//eof
