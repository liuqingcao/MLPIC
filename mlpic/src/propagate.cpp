#include "propagate.h"

//////////////////////////////////////////////////////////////////////////////////////////

propagate::propagate(parameter &p, domain &grid)
    : input(p),
      stk(p),
      rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("propagate::Constructor",errname);

  Gamma         = p.Gamma;
  dx            = grid.dx;
  idx           = 1.0/dx;
  dt            = dx / Gamma;         // <--------------------- LT -----------------------
  domain_number = p.domain_number;

  n_domains   = input.n_domains;

  start_time  = input.start_time;
  stop_time   = input.stop_time;

  ofstream outfile;
  outfile.open(p.outname,ios::app);

  outfile << "propagate constructor" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Gamma              : " << Gamma         << endl;
  outfile << "dx                 : " << dx            << endl;
  outfile << "idx                : " << idx           << endl;
  outfile << "dt                 : " << dt            << endl;
  outfile << "domain             : " << domain_number << endl << endl << endl;

  outfile.close();
}

//////////////////////////////////////////////////////////////////////////////////////////


input_propagate::input_propagate( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_propagate::Constructor",errname);

  rf.openinput( p.input_file_name );

  start_time  = atoi( rf.setget( "&propagate", "prop_start" ) );
  stop_time   = atoi( rf.setget( "&propagate", "prop_stop"  ) );

  n_domains   = 1;

  Q_restart   = 0;


  rf.closeinput();

  bob.message( "parameter read" );

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_propagate::save( parameter &p )
{
  static error_handler bob("input_propagate::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "propagate" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q_restart          : " << Q_restart      << endl;
  outfile << "restart_file       : " << restart_file   << endl;
  outfile << "prop_start         : " << start_time     << endl;
  outfile << "prop_stop          : " << stop_time      << endl;
  outfile << "N_domains          : " << n_domains      << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void propagate::loop(parameter &p, box &sim, pulse &laser_front, pulse &laser_rear,
                     diagnostic &diag )
{
  static error_handler bob("propagate::loop",errname);

  //////////////////////////////////// clocks ////////////////////////////////////////////
  uhr            zeit(p,"total"     );                                                  //
  uhr  zeit_particles(p,"particles" );                                                  //
  uhr     zeit_fields(p,"fields"    );                                                  //
  uhr zeit_diagnostic(p,"diagnostic");                                                  //
  ////////////////////////////////////////////////////////////////////////////////////////

  zeit.start();
	cout<<endl;
  for( time = start_time; time <= stop_time + dt; time += dt )
    {


	  float prog = (time - start_time)*100.0/(stop_time - start_time);
	  int pross = int(prog*100000)%1000000;
	  if(pross == 0) cout<<prog<<"%-> "<<flush;
	  if(prog == 100) cout<<"Done"<<endl;

      clear_grid( sim.grid );

      zeit_particles.start();
      particles( sim.grid );            // accelerate and move
      reflect_particles( sim.grid );    // reflect particles at box boundaries
      zeit_particles.stop_and_add();

      zeit_fields.start();
      fields( sim.grid, laser_front, laser_rear );// propagate fields
      zeit_fields.stop_and_add();

      zeit_diagnostic.start();
      diag.out( time, &(sim.grid), p ); // diagnostics
      diag.count();                     // diagnostic counter
      zeit_diagnostic.stop_and_add();

      zeit.add();                       // update clock
    }

  zeit.stop_and_add();

  zeit_particles.seconds_cpu();
  zeit_fields.seconds_cpu();
  zeit_diagnostic.seconds_cpu();
  zeit.seconds_cpu();
  zeit.seconds_sys();
}


//////////////////////////////////////////////////////////////////////////////////////////


void propagate::clear_grid( domain &grid )
{
  static error_handler bob("propagate::clear_grid",errname);

  struct cell *cell;

  for( cell=grid.Lbuf; cell!=grid.dummy; cell=cell->next )
    {
      cell->charge  = 0;
      cell->dens[0] = 0;
      cell->dens[1] = 0;
      cell->jx      = 0;
      cell->jy      = 0;
      cell->jz      = 0;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
//EOF



