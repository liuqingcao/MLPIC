#ifndef DOMAIN_H
#define DOMAIN_H

#include "common.h"
#include <fstream>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include "error.h"
#include "cell.h"
#include "particle.h"
#include "parameter.h"
#include "readfile.h"

using namespace std;

class input_domain {
private:
  char errname[filename_size];

public:
  int      Q_restart;           // start from t>0, using restart files
  char     restart_file[filename_size];
  int      Q_restart_save;

  int      n_domains;           // grid
  int      cells;
  int      cells_per_wl;

  double   dx;



  int      nsp;                 // particles
  int      *ppc_e;
  int      *fix_e;
  int      *ppc_i;
  int      *fix_i;  
  double   *z_e, *z_i, *zmax, *m_e, *m_i;
  double   *vtherm_e;
  double   *vtherm_i;  
  
  

  int      *cells_left;
  int      *cells_ramp;
  int      *cells_plasma; 

  double   *n_ion_over_nc;       // plasma density
  double   *n_el_over_nc;




  double   angle;                    // angles of incidence
  double   Gamma, Beta;              // Lorentz transformation
  int      spp;                      // steps per laser period

  readfile rf;
  input_domain( parameter &p );
  void save( parameter &p );
};

//////////////////////////////////////////////////////////////////////////////////////////

class domain {
 private:

  char         errname[filename_size];
  int          domain_number;
  int          n_domains;
  char         path[filename_size];
  input_domain input;

  void        set_boundaries( void );
  void           chain_cells( void );
  void            init_cells( void );
  void       chain_particles( void );
  void        init_particles( void );
  double        gauss_rand48( void );

public:

  int         n_left;     // cell number at the left boundary
  int         n_right;    // cell number at the right boundary
  int         n_cells;    // number of cells in this domain

  double dx;              // cell width

  struct cell *Lbuf;      // lhs: left buffer cell  left of left
  struct cell *lbuf;      //      right buffer      left of left
  struct cell *left;      // pointer to the first occupied cell
  struct cell *right;     // pointer to the last occupied cell
  struct cell *rbuf;      // rhs: left buffer cell  right of right
  struct cell *Rbuf;      //      right buffer      right of right
  struct cell *dummy;     // definitely the last one

  int n_el_spec[100];               // # of electrons
  int n_ion_spec[100];              // # of ions
  
  
  int n_el, n_ion;
  int n_part;             //  # particles

                    domain( parameter &p );
  void     count_particles( void );
  void               check( void );

};


#endif



