#ifndef ERROR_H
#define ERROR_H

#include "common.h"
#include <fstream>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <cstdlib>
#include <iostream> 

using namespace std;

class error_handler {
    static int error_number;
    static int message_number;
    static int Q_debug;
    static int debug_number;
    static int object_number;
    const char *my_name;
    char       *errname;
    ofstream   errfile;
    static int tab;
public:
    error_handler(const char *, char *error_file_name);
    void error(char* s1,    char*  s2="",
	       char* s3="", char*  s4="");
    void error(char* s1,    double d2,
	       char* s3="", char*  s4="");

    void message(char* m1, 
		 char* m2="", char*  m3="", char* m4="");
    void message(char* m1,    double m2,
		 char* m3="", char*  m4="");
    void message(char* m1,    double m2,    char* m3, double m4);
    void message(char* m1,    double m2,    char* m3, double m4,
		 char* m5,    double m6,    char* m7, double m8);
    void message(char* m1, double m2, double m3, double m4, double m5 );
    void message(char* m1, double m2, double m3, double m4 );
    void message(char* m1, double m2, char* m3,  double m4, char* m5, double m6);
    void message(char *s1, double d2, double d3);
    void message(char *s1, char *s2, double d3);

    void debug(char* m1, 
	       char* m2="", char*  m3="", char* m4="");
    void debug(char* m1,    double m2,
	       char* m3="", char*  m4="");
    void debug(char* m1   , double m2,    char* m3, double m4);
    void debug(char* m1   , double m2,    char* m3, double m4, char* m5, double m6);
};

#endif







