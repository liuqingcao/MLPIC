#include "diagnostic_particles.h"


//////////////////////////////////////////////////////////////////////////////////////////

el_dgparticle::el_dgparticle( parameter &p,
			      int species_input, char *species_name_input )
  : dgparticle(p,species_input,species_name_input)
{
}

ion_dgparticle::ion_dgparticle( parameter &p,
				int species_input, char *species_name_input )
  : dgparticle(p,species_input,species_name_input)
{
}

//////////////////////////////////////////////////////////////////////////////////////////


dgparticle::dgparticle( parameter &p,
			int species_input, char *species_name_input )
  : rf(),
    input(p,species_name_input),
    stepper( input.stepper, p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler shasha("dgparticle::Constructor",errname);

  species = species_input;
  sprintf( species_name, "%s", species_name_input );
}


//////////////////////////////////////////////////////////////////////////////////////////


input_dgparticle::input_dgparticle( parameter &p, char *species_name_input )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler shasha("input_dgparticle::Constructor",errname);
  char input_name[filename_size];

  sprintf( species_name, "%s", species_name_input );

  rf.openinput( p.input_file_name );

  cells             = atoi( rf.setget( "&box", "cells" ) );
  cells_per_wl      = atoi( rf.setget( "&box", "cells_per_wl" ) );

  sprintf( input_name, "&%s_particle", species_name );
  stepper.Q         = atoi( rf.setget( input_name, "Q" ) );
  stepper.t_start   = atof( rf.setget( input_name, "t_start" ) );
  stepper.t_stop    = atof( rf.setget( input_name, "t_stop" ) );
  stepper.t_step    = atof( rf.setget( input_name, "t_step" ) );

  stepper.x_start   = -1;   // not used
  stepper.x_stop    = -1;   // not used
  stepper.x_step    = -1;   // not used

  Q_restart       = 0;

  rf.closeinput();

  if(stepper.Q == 1) system("mkdir -p ./data/particle");

  shasha.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_dgparticle::save( parameter &p )
{
  static error_handler bob("input_particle::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic " << species_name << "_dgparticle" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void dgparticle::write_dgparticle( double time, parameter &p, domain *grid )
{
  static error_handler shasha("dgparticle::write_dgparticle",errname);

	int nsp = p.nsp;

  struct cell *cell;
  struct particle *part;

	for(int j=0;j<nsp;j++)
	{
  name=new char[filename_size];
  sprintf(name,"%s/particle/particle-%d-sp%d-%.3f", p.path, j+1, species, time);
  ofstream fout;
  fout.open(name);
  fout.precision(16);
  fout.setf( ios::showpoint | ios::scientific );



fout<<"number"<<setw(8)
	<<"x"<<setw(8)
	<<"vx"<<setw(8)
	<<"vy"<<setw(8)
	<<"vz"<<setw(13)
	<<"absolut"<<setw(13)
	<<"Gamma"<<setw(13)
	<<"ex"<<setw(8)
	<<"ey"<<setw(8)
	<<"ez"<<setw(8)
	<<"by"<<setw(8)
	<<"bz"<<endl;

  for( cell=grid->left; cell!=grid->rbuf; cell=cell->next ) {

      if (cell->npart != 0) 
      {
		  
	ex = cell->ex;
	ey = cell->ey;
	ez = cell->ez;
	by = cell->by;
	bz = cell->bz;
	
	for( part=cell->first; part!=NULL; part=part->next ) {
 
	    if (part->species == species && part->target == j+1  ) 
	    {
	      number = part->number;


     //     ux = part->ux;
	 //     uy = part->uy;
	  //    uz = part->uz;

		  Beta       = p.Beta;
		  Gamma      = p.Gamma;

		  x =  part->x;
 	      vx = part->ux * part->igamma;
	      vy = part->uy * part->igamma;
	      vz = part->uz * part->igamma;

	      vx = 1.0/Gamma * vx / ( 1 + vy * Beta );
	      vz = 1.0/Gamma * vz / ( 1 + vy * Beta );
	      vy = ( vy + Beta ) / ( 1 + vy * Beta );
	      absolut = sqrt( sqr(vx) + sqr(vy) + sqr(vz) );


          fout<<number<<"   "<<x<<"   "<<vx<<"   "<<vy<<"   "<<vz<<"   "<<absolut<<"   "<<Gamma<<"   "
              <<ex<<"   "<<ey<<"   "<<ez<<"   "<<by<<"   "<<bz<<"   "<<endl;

	    }
	  }
      }
    }

     fout.close();
 }
     
}


//////////////////////////////////////////////////////////////////////////////////////////
//eof




