#ifndef STACK_H
#define STACK_H

#include "common.h"
#include "cell.h"
#include "particle.h"
#include "error.h"
#include "parameter.h"
using namespace std;
typedef struct stack_member_struct {
  struct stack_member_struct *next;
  struct particle            *part;
  struct cell                *new_cell;
} stack_member;

class stack {

 private:
  char errname[filename_size];

 public:
                   stack( parameter &p );
  void      put_on_stack( struct cell *new_cell, struct particle *part );
  void remove_from_stack( stack_member *member );
  void   insert_particle( struct cell *new_cell, struct particle *part ); 

  stack_member *zero;
  stack_member *hole;
};

#endif


