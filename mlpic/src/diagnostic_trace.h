#ifndef DIAGNOSTIC_TRACE_H
#define DIAGNOSTIC_TRACE_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>
#include "readfile.h"

using namespace std;

class input_trace {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper;
  int           domain_number;
  int           traces;
  int           *tracepos;
  float         *trace_real_pos;
  int           Q_restart;
  char          restart_file[filename_size];
  int           Q_restart_save;
  char          restart_file_save[filename_size];
  char          *path;

  input_trace( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class trace {
private:
  char        errname[filename_size];
  readfile    rf;
  input_trace input;

public:
  diagnostic_stepper stepper;

  int         traces;
  int         *cell_number;
  struct cell **cell;
  float       **fp, **fm, **gp, **gm;
  float       **ey, **bz, **ez, **by, **ex;  
  float       **dens_e, **dens_i;
  float       **jx, **jy, **jz;
  char        *name;
   ofstream file; 
  float       Gamma,Beta;

  trace             ( parameter &p );
  void store_traces ( domain* grid );
  void write_traces ( double time, parameter &p );
  void restart_save ( void );
  float  average_vx ( struct cell* cell);
  float  average_vy ( struct cell* cell);
  float  average_vz ( struct cell* cell);  
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
