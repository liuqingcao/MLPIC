#include "propagate.h"

void propagate::fields( domain &grid, pulse &laser_front, pulse &laser_rear )
{
  static error_handler bob("propagate::fields", errname);

  struct cell *cell;
  double fp, fp_old, gm, gm_old;
  double pidt = PI * dt;

  cell=grid.left;

  fp_old   = cell->fp;
  gm_old   = cell->gm;

  if (domain_number==1) {
    cell->fp = laser_front.fieldey( time );
    cell->gm = laser_front.fieldez( time );
  }

  cell->fm = cell->next->fm - pidt * cell->jy;
  cell->gp = cell->next->gp - pidt * cell->jz;
  
  cell->ey = cell->fp + cell->fm;   cell->ez = cell->gp + cell->gm;
  cell->bz = cell->fp - cell->fm;   cell->by = cell->gp - cell->gm;
  
  cell->ex -= 2.0 * pidt * cell->jx;

  for( cell=grid.left->next; cell->next!=grid.rbuf; cell=cell->next )
    {
      fp = cell->fp;                                   // could be done in a more elegant 
      cell->fp = fp_old - pidt * cell->prev->jy;       // way: i.e. do the loop twice, 
      fp_old = fp;                                     // forward for fm and gp, 
                                                       // backward for fp and gm
      gm = cell->gm;
      cell->gm = gm_old - pidt * cell->prev->jz;
      gm_old = gm;

      cell->fm = cell->next->fm - pidt * cell->jy;
      cell->gp = cell->next->gp - pidt * cell->jz;
      
      cell->ey = cell->fp + cell->fm;   cell->bz = cell->fp - cell->fm;
      cell->ez = cell->gp + cell->gm;   cell->by = cell->gp - cell->gm;
      
      cell->ex -= 2.0 * pidt * cell->jx;
    }

  cell->fp = fp_old - pidt * cell->prev->jy;
  cell->gm = gm_old - pidt * cell->prev->jz;


    cell->fm = laser_rear.fieldey( time );
    cell->gp = laser_rear.fieldez( time );
  

  
  cell->ey = cell->fp + cell->fm;   cell->bz = cell->fp - cell->fm;
  cell->ez = cell->gp + cell->gm;   cell->by = cell->gp - cell->gm;
  
  cell->ex -= 2*pidt * cell->jx;
}


//////////////////////////////////////////////////////////////////////////////////////////
//EOF
