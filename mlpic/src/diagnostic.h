#ifndef DIAGNOSTIC_H
#define DIAGNOSTIC_H

#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>
#include "readfile.h"

#include "diagnostic_stepper.h"
#include "diagnostic_trace.h"
#include "diagnostic_spacetime.h"
#include "diagnostic_energy.h"
#include "diagnostic_reflex.h"
#include "diagnostic_flux.h"
#include "diagnostic_snapshot.h"
#include "diagnostic_velocity.h"
#include "diagnostic_particles.h"
#include "diagnostic_poisson.h"
#include "diagnostic_trail.h"
//#include "diagnostic_distribution.h"

using namespace std;

class input_diagnostic {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  int Q_restart;
  char restart_file[filename_size];
  int Q_restart_save;
  char restart_file_save[filename_size];

  input_diagnostic( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class diagnostic {

private:
  
  readfile         rf;
  input_diagnostic input;

  int              time_steps, time_out;

  int              domain_number;
  char             errname[filename_size];
  char             output_path[filename_size];

public:

  diagnostic ( parameter &p, domain* grid );
  void             out( double time, domain* grid, parameter &p );
  void           count( void );
  int     write_window( int time_steps, diagnostic_stepper *stepper );
  int write_window_pha( double time, diagnostic_stepper *stepper );

  int     public_time_steps;
  int     time_out_count;
  double  delta_t;

  poisson        poi;
  snapshot       sna;
  el_velocity    vel_el;
  ion_velocity   vel_ion;
  flux           flu;
  reflex         ref;
  spacetime      spa;
  energy         ene;
  trace          tra;
  el_dgparticle  par_el;
  ion_dgparticle par_ion;
  el_trail          trai_el;
  ion_trail          trai_ion;

};

//////////////////////////////////////////////////////////////////////////////////////////

#endif



