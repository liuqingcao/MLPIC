//////////////////////////////////////////////////////////////////////////////////////////
//
// laser pulses : linear, sin, sin^2
//
//////////////////////////////////////////////////////////////////////////////////////////


#ifndef PULSE_H
#define PULSE_H

#include "common.h"
#include <cmath>
#include "error.h"
#include "parameter.h"
#include <fstream>
#include <iomanip>
#include "readfile.h"
using namespace std;
//////////////////////////////////////////////////////////////////////////////////////////

class input_pulse {
private:
  char errname[filename_size];

public:
  int    Q1,Q2;

  int    shape1,shape2;         // 1=linear, 2=sin, 3=sinsqr
  int    polarization1,polarization2;  // 1=s, 2=p, 3=circular
  double a01,a02;            // dimensionless amplitude of 1omega light
  double c1,c2;             // chirp
  double raise1,raise2;         // raise time in periods
  double duration1,duration2;      // pulse duration in periods
  double a21,a22,a31, a32;        // amplitudes of 2omega and 3omega
  double p11, p21,p22, p31,p32;        // phases of 2omega and 3omega

  double omega2;
  double delaytime,delayphase;          // delay between pulse1 and pulse2 in unit of period

  int    Q_save;
  double save_step;

  double time_start, time_stop;  // simulation time interval in periods

  int    Q_restart;				      
  
  readfile rf;
  input_pulse( parameter &p, char *side, int pulse_number );
  void save( parameter &p, int pulse_number );
};

//////////////////////////////////////////////////////////////////////////////////////////

class pulse {

private:

  static int pulse_number; // counts elements of type pulse

  input_pulse input;

  char   errname[filename_size];
  char   *path;

public:

  double Qy1,Qy2;            // polarization dependent
  double Qz1,Qz2;            // "
  double shift1,shift2;         // relative shift between y and z for circular polarization


            pulse( parameter &p, char *side );
  void       save( double t_start, double t_stop, double t_step );
  double    fieldey( double t );
  double    fieldez( double t );  
  double envelope1( double t );
  double envelope2( double t );  
};


//////////////////////////////////////////////////////////////////////////////////////////

#endif


