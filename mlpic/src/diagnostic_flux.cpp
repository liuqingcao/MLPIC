#include "diagnostic_flux.h"


//////////////////////////////////////////////////////////////////////////////////////////


flux::flux( parameter &p )
  : rf(),
    input(p),
    stepper( input.stepper, p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("flux::Constructor",errname);

  if( input.Q_restart == 0 ){
    if(stepper.Q){
      name  = new( char [filename_size] );
      sprintf(name, "%s/flux", p.path);
    
      file.open(name,ios::out);
      if (!file) bob.error( "cannot open file", name );
      
      file.precision( 3 );
      file.setf( ios::showpoint | ios::scientific );
    
      file << "#" << setw(11) << "time" 
	   << setw(14) << "S+ left"  << setw(12) << "S- left" 
	   << setw(14) << "S+ right" << setw(12) << "S- right" << endl;
      
      file.close();
    }
  }
  else{
    if(stepper.Q){
      name  = new( char [filename_size] );
      sprintf(name, "%s/flux", p.path);

      file.open(name,ios::app);
      if (!file) bob.error( "cannot open file", name );
    
      file.precision( 3 );
      file.setf( ios::showpoint | ios::scientific );
      file << endl;
      file.close();

      char fname[ filename_size ];
      sprintf( fname, "%s/%s-%d-data1", p.path, input.restart_file, p.domain_number );
      rf.openinput(fname); 
      stepper.t_count = atoi( rf.getinput( "flu.stepper.t_count" ) );
      rf.closeinput();
    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////


input_flux::input_flux( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_flux::Constructor",errname);

  rf.openinput( p.input_file_name );

  stepper.Q         = atoi( rf.setget( "&flux", "Q" ) );
  stepper.t_start   = atof( rf.setget( "&flux", "t_start" ) );
  stepper.t_stop    = atof( rf.setget( "&flux", "t_stop" ) );
  stepper.t_step    = atof( rf.setget( "&flux", "t_step" ) );
  stepper.x_start   = -1;   // not used
  stepper.x_stop    = -1;   // not used
  stepper.x_step    = -1;   // not used

  Q_restart       = 0;

  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_flux::save( parameter &p )
{
  static error_handler bob("input_flux::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic flux" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void flux::write_flux( double time, domain *grid )
{
  static error_handler bob("flux::write_flux",errname);

  file.open(name,ios::app);
  if (!file) bob.error( "cannot open file", name );

  file.precision( 3 );
  file.setf( ios::showpoint | ios::scientific );

  file << setw(12) << time 
              << setw(14) << sqr(grid->left->fp) + sqr(grid->left->gm) 
              << setw(12) << sqr(grid->left->fm) + sqr(grid->left->gp)
              << setw(14) << sqr(grid->right->fp) + sqr(grid->right->gm) 
	      << setw(12) << sqr(grid->right->fm) + sqr(grid->right->gp) << endl;

  file.close();
}


//////////////////////////////////////////////////////////////////////////////////////////
//eof
