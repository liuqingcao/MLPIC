//
// this file should be always under construction
// any diagnostic can be added, replaced or removed
//
#include "diagnostic.h"

diagnostic::diagnostic( parameter &p, domain* grid )
  : rf(),
    input(p),
    poi(p,grid),
    sna(p),
    vel_el(p),
    vel_ion(p),
    flu(p),
    ref(p),
    spa(p),
    ene(p,grid),
    tra(p),
    trai_el(p),
    trai_ion(p),
    par_el(p),
    par_ion(p)
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("diagnostic::Constructor",errname);

  time_out          = p.spp;
  domain_number     = p.domain_number;
  delta_t           = 1.0/p.spl/p.Gamma;
  //cout<<setprecision(16);
  //cout<<delta_t<<endl<<grid->dx/p.Gamma;

  strcpy(output_path,p.path);

  if(input.Q_restart == 0){
    time_steps        = 0;
    time_out_count    = time_out;
  }
  public_time_steps = time_steps;
}


//////////////////////////////////////////////////////////////////////////////////////////


input_diagnostic::input_diagnostic( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_diagnostic::Constructor",errname);

  rf.openinput( p.input_file_name );

  Q_restart         = 0;

  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_diagnostic::save( parameter &p )
{
  static error_handler bob("input_diagnostic::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl;
  outfile << "Q_restart_save   : " << Q_restart_save  << endl;
  outfile << "restart_file_save: " << restart_file_save << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void diagnostic::count( void )
{
  static error_handler bob("diagnostic::count",errname);

  time_steps     ++;
  time_out_count ++;
  public_time_steps = time_steps;

  ene.stepper.t_count  ++;
  flu.stepper.t_count  ++;
  sna.stepper.t_count  ++;
  par_el.stepper.t_count  ++;
  par_ion.stepper.t_count  ++;
  vel_el.stepper.t_count  ++;
  vel_ion.stepper.t_count  ++;
  ref.stepper.t_count  ++;
  poi.stepper.t_count  ++;
  tra.stepper.t_count  ++;
  spa.stepper_de.t_count    ++;
  spa.stepper_di.t_count    ++;
  spa.stepper_jx.t_count    ++;
  spa.stepper_jy.t_count    ++;
  spa.stepper_jz.t_count    ++;
  spa.stepper_ex.t_count    ++;
  spa.stepper_ey.t_count    ++;
  spa.stepper_ez.t_count    ++;
  spa.stepper_bx.t_count    ++;
  spa.stepper_by.t_count    ++;
  spa.stepper_bz.t_count    ++;
  spa.stepper_edens.t_count ++;
  trai_el.stepper.t_count  ++;
  trai_ion.stepper.t_count  ++;
}


//////////////////////////////////////////////////////////////////////////////////////////


int diagnostic::write_window( int time_steps, diagnostic_stepper *stepper )
{
  int ok;
  if ( stepper->Q ) {                                // this diagnostic switched on ?
    if ( time_steps == stepper->t_start )
      stepper->t_count = stepper->t_step;            // start writing now !
    if ( time_steps >= stepper->t_start      &&      // inside time window and at
         time_steps <  stepper->t_stop       &&      //        point of time for writing ?
	 stepper->t_count == stepper->t_step    ) {  // yes:
      stepper->t_count = 0;                          //      reset write counter
      ok = 1;                                        //      return ok
    }
    else ok = 0;                                     // no:  do not write
  }
  else ok = 0;                                       // do not write

  return ok;
}



//////////////////////////////////////////////////////////////////////////////////////////


int diagnostic::write_window_pha( double time, diagnostic_stepper *stepper )
{
//  char c;
//  cout<<setprecision(16)<<"write_window_pha time:"<<time<<endl
//      <<"stepper->t_count_pha :"<<stepper->t_count_pha<<endl
//      <<"stepper->flag_pha:"<<stepper->flag_pha<<endl;
//  if(stepper->flag_pha==1) c=getchar();

  int ok;

  if ( stepper->Q ) {
    if ( abs(time - stepper->t_start_pha) < TINY )  {
       ok = 1;
       stepper->t_count_pha = stepper->t_step_pha;
       return ok;
    }
    if ( (time > stepper->t_start_pha || abs(time - stepper->t_start_pha) < TINY ) &&      // inside time window and at
         time <  stepper->t_stop_pha       &&
	     abs(time-stepper->t_count_pha)<0.5*delta_t   )  {
            if(  time-stepper->t_count_pha <0  ) {
               stepper->flag_pha = 1;
               ok = 0;
               return ok;
            }
            else {
               ok = 1;
               stepper->t_count_pha += stepper->t_step_pha;
               return ok;
            }
    }
    else if( stepper->flag_pha == 1 ) {
       stepper->flag_pha = 0;
       ok = 1;
       stepper->t_count_pha += stepper->t_step_pha;
       return ok;
    }
  }
  else { ok = 0;  return ok; }

}



//////////////////////////////////////////////////////////////////////////////////////////


void diagnostic::out( double time, domain* grid, parameter &p )
{
  static error_handler bob("diagnostic::out",errname);

  int ok_spa;

  if ( time_out_count == time_out ) {
    bob.message( "---------- TIME =", time, "----------" );
    time_out_count = 0;
  }


  // ---- traces -------------------------------------------------------------------------

  if ( tra.stepper.Q ) {
    if ( time_steps == tra.stepper.t_start )
         tra.stepper.t_count = 0;
    if (    time_steps >= tra.stepper.t_start
	 && time_steps <= tra.stepper.t_stop
         && tra.stepper.t_count == tra.stepper.t_step ) {
      tra.write_traces(time,p);
      tra.stepper.t_count = 0;
    }
  }

  if ( tra.stepper.Q ) {                          // store in memory
    if (    time_steps >= tra.stepper.t_start
	 && time_steps <= tra.stepper.t_stop )
	    tra.store_traces(grid);
  }

  // ---- trail -----------------------------------------------------------------------------

    if ( write_window(time_steps,&(trai_el.stepper)) )
    trai_el.store_trail(time,grid,p);
    if ( write_window(time_steps,&(trai_ion.stepper)) )
    trai_ion.store_trail(time,grid,p);

  // ---- energy, flux, reflectivity -----------------------------------------------------

  ene.average_reflex(grid);

  if ( write_window(time_steps,&(ene.stepper)) ) {
    ene.get_energies(grid);
    ene.write_energies(time);
  }

  if ( write_window(time_steps,&(flu.stepper)) )
    flu.write_flux(time,grid);

  ref.average_reflex(grid);

  if ( write_window(time_steps,&(ref.stepper)) )
    ref.write_reflex(time);

  // ---- snapshot -----------------------------------------------------------------------

  if ( write_window(time_steps,&(sna.stepper)) )
    sna.write_snap(time,grid,p);

  // ---- dgparticle ---------------------------------------------------------------------

  if ( write_window(time_steps,&(par_el.stepper)) )
    par_el.write_dgparticle(time,p,grid);

  if ( write_window(time_steps,&(par_ion.stepper)) )
    par_ion.write_dgparticle(time,p,grid);


  // ---- velocity -----------------------------------------------------------------------

  if ( write_window(time_steps,&(vel_ion.stepper)) )
    vel_ion.write_velocity(time,p,grid);

  if ( write_window(time_steps,&(vel_el.stepper)) )
    vel_el.write_velocity(time,p,grid);

  // ---- poisson ------------------------------------------------------------------------

  if ( write_window(time_steps,&(poi.stepper)) ) {
    poi.solve(grid);
    poi.write(time,grid);
  }

  //---------------------- de ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_de));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_de.Q ) spa.write_de(time,grid,time_out_count,ok_spa,p);

  //---------------------- di ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_di));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_di.Q ) spa.write_di(time,grid,time_out_count,ok_spa,p);

  //---------------------- jx ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_jx));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_jx.Q ) spa.write_jx(time,grid,time_out_count,ok_spa,p);

  //---------------------- jy ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_jy));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_jy.Q ) spa.write_jy(time,grid,time_out_count,ok_spa,p);

  //---------------------- jz ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_jz));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_jz.Q ) spa.write_jz(time,grid,time_out_count,ok_spa,p);

  //---------------------- ex ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_ex));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_ex.Q ) spa.write_ex(time,grid,time_out_count,ok_spa,p);

  //---------------------- ey ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_ey));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_ey.Q ) spa.write_ey(time,grid,time_out_count,ok_spa,p);

  //---------------------- ez ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_ez));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_ez.Q ) spa.write_ez(time,grid,time_out_count,ok_spa,p);

  //---------------------- bx ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_bx));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_bx.Q ) spa.write_bx(time,grid,time_out_count,ok_spa,p);

  //---------------------- by ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_by));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_by.Q ) spa.write_by(time,grid,time_out_count,ok_spa,p);

  //---------------------- bz ------------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_bz));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_bz.Q ) spa.write_bz(time,grid,time_out_count,ok_spa,p);

  //---------------------- edens ---------------------------------------------------------

  ok_spa=write_window(time_steps,&(spa.stepper_edens));
  if ( (ok_spa || time_out_count == 1)&&spa.stepper_edens.Q ) spa.write_edens(time,grid,time_out_count,ok_spa,p);
}



//////////////////////////////////////////////////////////////////////////////////////////
//eof



