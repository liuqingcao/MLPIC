#include "main.h"


//////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    // initialize classes ////////////////////////////////////////////////////////////////
    parameter p(argc,argv);                         // read parameters
    char                 errname[filename_size];
    sprintf( errname, "%s/error", p.path);
    static error_handler bob("main",errname);       // error handler for main.C

    box        sim(p);                              // init domain, cells, particles
                                                   // spawn task for the following domain

    pulse      laser_front(p,"&pulse_front");       // init laser pulses
    pulse      laser_rear(p,"&pulse_rear");

    diagnostic diag(p,&(sim.grid));                 // init diagnostics

    propagate  prop(p,sim.grid);                    // init propagator

    // main loop /////////////////////////////////////////////////////////////////////////
    prop.loop(p,sim,laser_front,laser_rear,diag);
    // exit //////////////////////////////////////////////////////////////////////////////
    
    return 1;


}

//////////////////////////////////////////////////////////////////////////////////////////
//EOF


