#ifndef DIAGNOSTIC_DGPARTICLE_H
#define DIAGNOSTIC_DGPARTICLE_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>
#include <fstream>
#include "readfile.h"

using namespace std;


class input_dgparticle {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );
  char          species_name[filename_size];

public:
  stepper_param stepper;
  int           n_domains;
  int           cells, cells_per_wl;
  int           Q_restart;
  char          restart_file[filename_size];

  input_dgparticle( parameter &p, char *species_name_input );
};


//////////////////////////////////////////////////////////////////////////////////////////


class dgparticle {
private:
  char             errname[filename_size];
  readfile         rf;
  input_dgparticle input;
  int              species, target;
  char             species_name[filename_size];

public:
  diagnostic_stepper stepper;

  int number;
  double x,vx,vy,vz,absolut;
  double Beta, Gamma;
  double ex,ey,ez,by,bz;

  char *name;


  dgparticle           ( parameter &p,
			 int species_input, char *species_name_input );
  void write_dgparticle( double time, parameter &p, domain *grid );
};


class el_dgparticle : public dgparticle {
public:

  el_dgparticle( parameter &p,
		 int species_input=0, char *species_name_input="el" );
};


class ion_dgparticle : public dgparticle {
public:

  ion_dgparticle( parameter &p,
		  int species_input=1, char *species_name_input="ion" );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
