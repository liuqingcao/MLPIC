#ifndef DIAGNOSTIC_REFLEX_H
#define DIAGNOSTIC_REFLEX_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;

class input_reflex {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper; 
  int           Q_restart;
  char          restart_file[filename_size];

  input_reflex( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class reflex {
private:
  char         errname[filename_size];
  readfile     rf;
  input_reflex input;

public:
  diagnostic_stepper stepper;

  double   *buf;
  char     *name;
  ofstream file;

  reflex              ( parameter &p );
  void average_reflex ( domain* grid );
  void write_reflex   ( double time );
};  

//////////////////////////////////////////////////////////////////////////////////////////

#endif
