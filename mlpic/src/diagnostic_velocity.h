#ifndef DIAGNOSTIC_VELOCITY_H
#define DIAGNOSTIC_VELOCITY_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;

class input_velocity {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );
  char          species_name[filename_size];

public:
  stepper_param stepper;
  int           Q_restart;
  char          restart_file[filename_size];

  input_velocity( parameter &p, char *species_name_input );
};


//////////////////////////////////////////////////////////////////////////////////////////


class velocity {
private:

  readfile       rf;
  input_velocity input;
  char           errname[filename_size];
  int            dim;
  int            *x, *y, *z, *a;
  double         Beta, Gamma, vcut;
  int            species;
  char           species_name[filename_size];

public:
  diagnostic_stepper stepper;

  char *name;
ofstream file;
  velocity ( parameter &p,
	     int species_input, char *species_name_input );
  void write_velocity( double time, parameter &p, domain *grid );
};


class el_velocity : public velocity {
public:

  el_velocity( parameter &p,
	       int species_input=0, char *species_name_input="el" );
};


class ion_velocity : public velocity {
public:

  ion_velocity( parameter &p,
		int species_input=1, char *species_name_input="ion" );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
