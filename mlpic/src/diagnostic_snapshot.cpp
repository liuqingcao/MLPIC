#include "diagnostic_snapshot.h"


//////////////////////////////////////////////////////////////////////////////////////////


snapshot::snapshot( parameter &p )
  : rf(),
    input(p),
    stepper( input.stepper, p )
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("snapshot::Constructor",errname);

  name  = new( char [filename_size] );

}


//////////////////////////////////////////////////////////////////////////////////////////


input_snapshot::input_snapshot( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_snapshot::Constructor",errname);

  rf.openinput( p.input_file_name );

  stepper.Q         = atoi( rf.setget( "&snapshot", "Q" ) );
  stepper.t_start   = atof( rf.setget( "&snapshot", "t_start" ) );
  stepper.t_stop    = atof( rf.setget( "&snapshot", "t_stop" ) );
  stepper.t_step    = atof( rf.setget( "&snapshot", "t_step" ) );
  stepper.x_start   = -1;   // not used
  stepper.x_stop    = -1;   // not used
  stepper.x_step    = -1;   // not used

  Q_restart       = 0;
  
  if(stepper.Q == 1) system("mkdir -p ./data/snapshot");
  
  rf.closeinput();

  bob.message("parameter read");

  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_snapshot::save( parameter &p )
{
  static error_handler bob("input_snapshot::save",errname);
  ofstream outfile;

  outfile.open(p.outname,ios::app);

  outfile << "diagnostic snapshot" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q                : " << stepper.Q       << endl;
  outfile << "t_start          : " << stepper.t_start << endl;
  outfile << "t_stop           : " << stepper.t_stop  << endl;
  outfile << "t_step           : " << stepper.t_step  << endl;
  outfile << "Q_restart        : " << Q_restart       << endl;
  outfile << "restart_file     : " << restart_file    << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void snapshot::write_snap( double time, domain* grid, parameter &p )
{
  static error_handler bob("snapshot::out_snap",errname);
  struct cell *cell;

  sprintf(name,"%s/snapshot/snap-%.3f", p.path, time);

  file.open(name);
  if (!file) bob.error("cannot open snapshot file", name );

  file.precision( 16 );
  file.setf( ios::showpoint | ios::scientific );

  file<< setw(5) << "x"
      << setw(10) << "Ex"  
      << setw(10) << "Ey"     
      << setw(10) << "Ez"      
      << setw(10) << "By"     
      << setw(10) << "Bz"      
	  << setw(15) << "rho_el" 
      << setw(15) << "rho_ion" 
	  << setw(10) << "jx" 
      << setw(10) << "jy"
      << setw(10) << "jz" 
      << setw(15) << "el_num"
      << setw(15) << "ion_num" << endl;

  for( cell=grid->left; cell!=grid->rbuf; cell=cell->next )
    {
     file << cell->x<<"   "
		   << cell->ex<<"   "
		<< cell->ey<<"   "
		<< cell->ez<<"   "
		<< cell->by<<"   "
		<< cell->bz<<"   "
		<< cell->dens[0]<<"   "
		<< cell->dens[1]<<"   "
        << cell->jx<<"   "
		<< cell->jy<<"   "
		<< cell->jz<<"   "
	    << cell->np[0]<<"   "
  	    << cell->np[1] << endl;
   }

  file.close(); 
}


//////////////////////////////////////////////////////////////////////////////////////////
//eof

