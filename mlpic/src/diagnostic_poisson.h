#ifndef DIAGNOSTIC_POISSON_H
#define DIAGNOSTIC_POISSON_H

#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>
#include "readfile.h"

using namespace std;

class input_poisson {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper; 
  int           n_domains;
  double        time_start, time_stop;
  int           Q_restart;
  char          restart_file[filename_size];

  input_poisson( parameter &p );
};


//////////////////////////////////////////////////////////////////////////////////////////


class poisson {

private:
  
  readfile      rf;
  input_poisson input;
  int           spp;
  int           domain_number;
  char          errname[filename_size];
  char          *output_path;

  void fft            ( double* data, int nn, int isign );
  double dif          ( double in );
  double smooth       ( double in );

public:
  diagnostic_stepper stepper;

  double *ex, *rhok, *phik;
  char *name;
  ofstream file;

       poisson ( parameter &p, domain *grid );
  void solve   ( domain* grid );
  void write   ( double time, domain* grid );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif
