#include "domain.h"
#include <iostream>
using namespace std;

domain::domain( parameter &p )
  : input(p)
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("domain::Constructor",errname);

  domain_number = p.domain_number;
  n_domains     = input.n_domains;
  dx            = input.dx;
  
  n_part = n_el = n_ion = 0; 
  
  strcpy( path, p.path );
	for(int i=0;i<100;i++)
	{
		n_el_spec[i]    = 0;                           // will be set in domain::chain_particles()
		n_ion_spec[i]   = 0;                           //  ''
	}

    // simulation box -----------------------

    set_boundaries();                      // determines boundaries from domain number   

    // init data structure-------------------

    chain_cells();                         // create chained list of cells
                                           // set cell numbers
                                           // set domain pointers to left and right cells

    init_cells();                     // set fields equal to zero
                                           // set normalized densities

    chain_particles();                     // allocate and link particles to cells
                                           // set number, species, cell, position
 
  // physical part ------------------------

    init_particles();                      // set charge, mass, velocities, gamma


    check();                               // check and save
                                          // particel numbers, positions, total charge
                                           // plot cell, x, densities, part. per cell
  


  bob.message( "sizeof(struct cell)     =", sizeof(struct cell), "Byte" );
  bob.message( "sizeof(struct particle) =", sizeof(struct particle), "Byte" );
}


//////////////////////////////////////////////////////////////////////////////////////////


input_domain::input_domain( parameter &p )
  : rf()
{
  sprintf( errname, "%s/error", p.path);
  static error_handler bob("input_domain::Constructor",errname);

  rf.openinput(p.input_file_name);

  Q_restart      = 0;


  // read grid ////////////////////////////////////////////////////

  n_domains    = p.n_domains;
  cells        = atoi( rf.setget( "&box", "cells" ) );
  cells_per_wl = atoi( rf.setget( "&box", "cells_per_wl" ) );
  dx           = 1.0 / cells_per_wl;
  
  
// read particles /////////////////////////////////////////////////
  nsp                 = p.nsp;

  fix_e                 = new(int[nsp]);
  fix_i                 = new(int[nsp]);  
  z_e                   = new(double[nsp]);
  m_e                   = new(double[nsp]);
  z_i                   = new(double[nsp]);
  m_i                   = new(double[nsp]);  
  ppc_e                 = new(int[nsp]);
  ppc_i                 = new(int[nsp]);  
  vtherm_e              = new(double[nsp]);
  vtherm_i              = new(double[nsp]);
  n_ion_over_nc         = new(double[nsp]);
  n_el_over_nc          = new(double[nsp]);
  cells_left             = new(int[nsp]);
  cells_ramp             = new(int[nsp]);
  cells_plasma             = new(int[nsp]);

for(int spec = 0;spec < nsp; spec++)
{
  char *species_name;
  species_name = new char [filename_size] ;
  sprintf( species_name, "&species%i", spec+1 );
  // position
  cells_left[spec]   = atoi( rf.setget( species_name, "cells_left" ) );
  cells_ramp[spec]   = atoi( rf.setget( species_name, "cells_ramp" ) );
  cells_plasma[spec] = atoi( rf.setget( species_name, "cells_plasma" ) ); 
  // electrons 
  fix_e[spec]         = atoi( rf.setget( species_name, "fix_e" ) );
  z_e[spec]           = -1;        // DEFAULT, SHOULD NOT BE CHANGED
  m_e[spec]           = +1;        // DEFAULT, SHOULD NOT BE CHANGED
  ppc_e[spec]         = atoi( rf.setget( species_name, "ppc_e" ) );
  vtherm_e[spec]      = atof( rf.setget( species_name, "vtherm_e" ) );
  // ions
  fix_i[spec]         = atoi( rf.setget( species_name, "fix_i" ) );
  z_i[spec]           = atoi( rf.setget(species_name, "z" ) );
  m_i[spec]           = atof( rf.setget( species_name, "m" ) );
  ppc_i[spec]         = atoi( rf.setget( species_name, "ppc_i" ) );
  vtherm_i[spec]      = atof( rf.setget( species_name, "vtherm_i" ) );
  n_ion_over_nc[spec] = atof( rf.setget( species_name, "n_ion_over_nc" ) );
  n_el_over_nc[spec] =  z_i[spec] * n_ion_over_nc[spec];

}

  // read spp, adjusted angle, Beta and Gamma

  spp         = p.spp;
  angle       = p.angle;
  Beta        = p.Beta;
  Gamma       = p.Gamma;

  rf.closeinput();

  bob.message("parameter read");





  if (p.domain_number==1) save(p);
}


//////////////////////////////////////////////////////////////////////////////////////////


void input_domain::save( parameter &p )
{
  static error_handler bob("input_domain::save",errname);
  ofstream outfile;
  int i;

  outfile.open(p.outname,ios::app);

  outfile << "domain: plasma" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "Q_restart          : " << Q_restart      << endl;
  outfile << "restart_file       : " << restart_file   << endl;
  outfile << "Q_restart_save     : " << Q_restart_save << endl;
  outfile << "N_domains          : " << n_domains      << endl;
  outfile << "cells              : " << cells          << endl;
  outfile << "cells_per_wl       : " << cells_per_wl   << endl;
  outfile << "dx                 : " << dx             << endl << endl;
  
  outfile << "domain: particles" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "nsp                : " << setw(8) << nsp << endl;
  outfile << "fix_e              : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << fix_e[i];
  outfile << endl  << "fix_i              : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << fix_i[i];  
  outfile << endl << "charge             : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << z_i[i];
  outfile << endl << "mass               : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << m_i[i];
  outfile << endl << "ppc_e              : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << ppc_e[i];
  outfile << endl << "ppc_i              : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << ppc_i[i];  
  outfile << endl << "vtherm_e           : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << vtherm_e[i];
  outfile << endl << "vtherm_i           : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << vtherm_i[i];   
  outfile << endl << "n_ion_over_nc      : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << n_ion_over_nc[i];  
  outfile << endl << "n_el_over_nc       : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << n_el_over_nc[i];  
  outfile << endl << "cells_left         : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << cells_left[i];   
  outfile << endl << "cells_ramp         : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << cells_ramp[i];   
  outfile << endl << "cells_plasma       : ";
  for(i=0;i<nsp;i++) outfile << setw(8) << cells_plasma[i];   

   
  outfile << endl << endl << endl;
  
  
  
  

  outfile << "domain: Lorentz transformation" << endl;
  outfile << "------------------------------------------------------------------" << endl;
  outfile << "spp                : " << spp             << endl;
  outfile << "adjusted angle     : " << angle           << endl;
  outfile << "Beta               : " << Beta            << endl;
  outfile << "Gamma              : " << Gamma           << endl << endl << endl;

  outfile.close();

  bob.message("parameter written");
}


//////////////////////////////////////////////////////////////////////////////////////////


void domain::set_boundaries()
// p.box.N domains of equal size
{
  error_handler bob("domain::set_boundaries",errname);

  int cells_per_domain = (int) floor( (double) input.cells / input.n_domains );

  n_left  = 1 + ( domain_number - 1 ) * cells_per_domain;
  n_right = n_left + cells_per_domain - 1;
  n_cells = cells_per_domain;

  if (domain_number==n_domains)
    {
      n_right = input.cells;
      n_cells = n_right - n_left + 1;
    }

  bob.message("n_left  = ", n_left );
  bob.message("n_right = ", n_right );
  bob.message("n_cells = ", n_cells );
}


//////////////////////////////////////////////////////////////////////////////////////////


void domain::chain_cells( void )
{
  error_handler bob("domain::chain_cells",errname);
  struct cell *cell_old, *cell_new;

  Lbuf         = new ( struct cell );
  if (!Lbuf) bob.error("allocation error: Lbuf");
  Lbuf->number = n_left - 2;

  lbuf         = new ( struct cell );
  if (!lbuf) bob.error("allocation error: lbuf");
  lbuf->number = n_left - 1;
  lbuf->prev   = Lbuf;

  left         = new ( struct cell );
  if (!left) bob.error("allocation error: left");
  left->number = n_left;
  left->prev   = lbuf;
  cell_old     = left;

  for( int i=n_left+1; i<=n_right; i++ )
    {
      cell_new         = new ( struct cell );
      if (!cell_new) bob.error("allocation error: cell_new");
      cell_new->prev   = cell_old;
      cell_old->next   = cell_new;
      cell_old         = cell_new;
      cell_new->number = i;
    }

  right         = cell_old;

  rbuf         = new ( struct cell );
  if (!rbuf) bob.error("allocation error: rbuf");
  rbuf->prev   = right;
  rbuf->number = n_right + 1;

  Rbuf         = new ( struct cell );
  if (!Rbuf) bob.error("allocation error: Rbuf");
  Rbuf->prev   = rbuf;
  Rbuf->number = n_right + 2;

  dummy         = new ( struct cell );
  if (!dummy) bob.error("allocation error: dummy");
  dummy->prev   = Rbuf;
  dummy->number = n_right + 3;

  right->next  = rbuf;
  rbuf->next   = Rbuf;
  Rbuf->next   = dummy;
  dummy->next  = Lbuf;
  Lbuf->next   = lbuf;
  Lbuf->prev   = dummy;
  lbuf->next   = left;

  // closed ring of cells with two buffer cells left and right and dummy cell
  // connecting Rbuf and Lbuf
}


void domain::init_cells(void)
{
  error_handler bob("domain::init_cells",errname);
  struct cell *cell;
  //initial the cell
  int nsp = input.nsp;
  for( cell=Lbuf; cell!=dummy; cell=cell->next )
  {
      cell->charge = 0;
      cell->jx = cell->jy = cell->jz = 0;
      cell->ex = cell->ey = cell->ez = 0;
      cell->bx = cell->by = cell->bz = 0;
      cell->fp = cell->fm = cell->gp = cell->gm = 0;
      
      cell->dens[0] = cell->dens[1] = 0;
      cell->np[0] = cell->np[1] = 0;
      cell->npart = 0;
        
	  for( int j=0;j<nsp; j++ )
	  {
		cell->dens_e[j] = cell->dens_i[j] = 0;
		cell->np_e[j] = 0;
		cell->np_i[j] = 0;  		
	  }
	}
		
  //
  
  
  
  if(nsp>0)
  {
	  for(int i = 0; i<nsp;i++)
	  for( cell=Lbuf; cell!=dummy; cell=cell->next )
		{
		  if (!cell) bob.error("allocation error");

		  cell->domain = domain_number;            // domain number
		  cell->x = dx * ( cell->number - 1 );     // cell coordinate, left boundary
		  // set up the normalized particle densities for a ramp profile
		  if (cell->number >= input.cells_left[i] && cell->number < input.cells_left[i] + input.cells_ramp[i])
		  {
			cell->dens_e[i] = (double)(cell->number - input.cells_left[i]) / input.cells_ramp[i];   
			cell->dens_i[i] = cell->dens_e[i];
		  }else if(cell->number >= input.cells_left[i] + input.cells_ramp[i] && cell->number < input.cells_left[i] + input.cells_plasma[i] )
		  {
			cell->dens_e[i] = cell->dens_i[i] = 1.0;
		  }
		}
	}else{
	  for( cell=Lbuf; cell!=dummy; cell=cell->next )
		{
		  cell->domain = domain_number;            // domain number
		  cell->x = dx * ( cell->number - 1 );     // cell coordinate, left boundary
	   	cell->dens_e[0] = cell->dens_i[0] = 0.0;
		} 
				
	}
      // will be set in domain::chain_particles()
}



//////////////////////////////////////////////////////////////////////////////////////////


void domain::chain_particles( void )
  // particle numbers are set in each domain separately, beginning with 1
  // this is corrected in the box::Constructor using communicate_particle_numbers(...)
  // in order to keep domain free of networking
{
  error_handler bob("domain::chain_particles",errname);

  int             i;
  int             number_e, number_i;         // count particles for each species sperately
  double          delta;
  struct cell     *cell;
  struct particle *pn, *po;
  int nsp = input.nsp;



  number_i = number_e = 0;



  for( cell=left; cell!=rbuf; cell=cell->next )   // for all cells including all buffers
    {
      cell->first = NULL;
      cell->last  = NULL;

      po = cell->first;

      for( int j=0; j<nsp; j++ )             // for all species
	{
		//electron
		cell->np_e[j] = (int) floor( cell->dens_e[j] * input.ppc_e[j] + 0.5 );
		n_el_spec[j] += cell->np_e[j];
		  if (cell->np_e[j]!=0)                      // for occupied cells
			{
			  delta  = dx / cell->np_e[j];

			  for( i=1; i<=cell->np_e[j]; i++ )      // for all particles of
			{                                  // kind j in this cell
			  number_e++;
			  pn          = new ( struct particle );
			  if (!pn) bob.error("allocation error");

			  pn->prev    = po;
			  if (po==NULL)
			  {
				  cell->first = pn;
			  }
			  else{
				   po->next    = pn;
			  }
			  
			  pn->number  = number_e;
			  pn->species = 0;
			  pn->target  = j+1;
			  pn->cell    = cell;
			  pn->x       = cell->x + ((double)i-0.50000001) * delta;

			  po          = pn;
			}
			}		
		
		//ion
		cell->np_i[j] = (int) floor( cell->dens_i[j] * input.ppc_i[j] + 0.5 );
		n_ion_spec[j] += cell->np_i[j];		
		//species all
		n_part += cell->np_e[j] + cell->np_i[j];
		  if (cell->np_i[j]!=0)                      // for occupied cells
			{
			  delta  = dx / cell->np_i[j];

			  for( i=1; i<=cell->np_i[j]; i++ )      // for all particles of
			{                                  // kind j in this cell
			  number_i++;
			  pn          = new ( struct particle );
			  if (!pn) bob.error("allocation error");

			  pn->prev    = po;
			  if (po==NULL)
			  {
				  cell->first = pn;
			  }
			  else{
				   po->next    = pn;
			  }
			  pn->number  = number_i;
			  pn->species = 1;
			  pn->target  = j+1;
			  pn->cell    = cell;
			  pn->x       = cell->x + ((double)i-0.50000001) * delta;

			  po          = pn;
			}
			}  
			//add
			 cell->npart += cell->np_e[j] + cell->np_i[j];
			 cell->np[0] += cell->np_e[j];
			 cell->np[1] += cell->np_i[j];
			 
			 
			 
			 
	 }
 
		
		  if (po!=NULL) 
		  {
			po->next = NULL;
			cell->last = po;
		   }
    }
    
    		 for( int j=0; j<nsp; j++ )
			{	n_el += n_el_spec[j];
				n_ion += n_ion_spec[j];
				cout<<"  nsp: "<<j+1<<"  electron: "<<n_el_spec[j]<<"  ion: "<<n_ion_spec[j]<<endl;
			}

}

//////////////////////////////////////////////////////////////////////////////////////////


void domain::init_particles( void )
{

  error_handler bob("domain::init_particles",errname);

  struct cell     *cell;
  struct particle *part;
  double Gamma   = input.Gamma;                   // gamma factor due to Lorentz transformation
  double Beta    = input.Beta;
  double vx, vy, vz;
  int nsp = input.nsp;
  
	for( cell=left; cell!=rbuf; cell=cell->next )   // for all cells
		if (cell->npart!=0)                         // for occupied cells
		{
			for( part=cell->first; part!=NULL; part=part->next )
			{                                      // for all particles in this cell
				if (!part) bob.error("allocation: part");
				int j = part->target-1;
				//electron
				if(part->species == 0)
				{
					part->fix     = input.fix_e[j];
					part->z       = input.z_e[j];
					part->m       = input.m_e[j];
					part->zm      = part->z / part->m;
					part->n  = pow(Gamma,3) * fabs( input.n_el_over_nc[j] ) / input.ppc_e[j];
					part->zn = part->z * part->n;
								   
					// thermal velocities
					do{
						vx            = input.vtherm_e[j] * gauss_rand48();
						vy            = input.vtherm_e[j] * gauss_rand48();
						vz            = input.vtherm_e[j] * gauss_rand48();
					}while(vx*vx+vy*vy+vz*vz>=1);
					// L-transform to the M frame
					vx            = vx * sqrt(1.0-Beta*Beta) / ( 1 - vy*Beta );
					vz            = vz * sqrt(1.0-Beta*Beta) / ( 1 - vy*Beta );
					vy            = ( vy - Beta ) / ( 1 - vy*Beta );

					// determine gamma*v
					part->igamma  = sqrt( 1.0 - vx*vx - vy*vy - vz*vz );
					part->ux      = vx / part->igamma;
					part->uy      = vy / part->igamma;
					part->uz      = vz / part->igamma;	            
				}
			//ion or neutral atoms
				else if(part->species == 1)
				{
					part->fix     = input.fix_i[j];
					part->z       = input.z_i[j];
					part->m       = input.m_i[j];
					part->zm      = part->z / part->m;
					if (part->z == 0) {     // neutral atoms
					part->n  = pow(Gamma,3) * input.n_ion_over_nc[j] / input.ppc_i[j];
					part->zn = 0;
					}else {                  // electrons or ions
					part->n  = pow(Gamma,3) * fabs( input.n_ion_over_nc[j] ) / input.ppc_i[j];
					part->zn = part->z * part->n;
					}          
					// thermal velocities
					do{
					vx            = input.vtherm_i[j] * gauss_rand48();
					vy            = input.vtherm_i[j] * gauss_rand48();
					vz            = input.vtherm_i[j] * gauss_rand48();
					}while(vx*vx+vy*vy+vz*vz>=1);
					// L-transform to the M frame
					vx            = vx * sqrt(1.0-Beta*Beta) / ( 1 - vy*Beta );
					vz            = vz * sqrt(1.0-Beta*Beta) / ( 1 - vy*Beta );
					vy            = ( vy - Beta ) / ( 1 - vy*Beta );

					// determine gamma*v

					part->igamma  = sqrt( 1.0 - vx*vx - vy*vy - vz*vz );
					part->ux      = vx / part->igamma;
					part->uy      = vy / part->igamma;
					part->uz      = vz / part->igamma;	            
				}
				else{
					cout<<"something is wrong"<<endl;
					exit(-1);
				}
			}
		}

    
    
    
    
 
}


//////////////////////////////////////////////////////////////////////////////////////////


double domain::gauss_rand48( void )
{
  double r1, r2;

  r1 = double((rand())%10000*0.0001);
  r2 = double((rand())%10000*0.0001);

  return sqrt( -2.0 * log( 1.0 - r1 ) ) * sin( 2*PI*r2 );
}


//////////////////////////////////////////////////////////////////////////////////////////


void domain::check( void )
{
  error_handler bob("domain::check_and_save",errname);
  struct cell *cell;
  struct particle *part;
  int count[2];
  double charge=0;
  int nsp = input.nsp;
  for( cell=Lbuf; cell!=dummy; cell=cell->next )
    {
      count[0] = count[1] = 0;

      if (cell->npart!=0)
	{
	  for( part=cell->first; part!=NULL; part=part->next )
	    {
	      if (!part) bob.error("allocation: part");
	      if ( part->x < cell->x || part->x > cell->x+dx )
		bob.error("particle position");
	      count[part->species]++;
	      charge += part->zn;
	    }
	}
    }

  if ( fabs(charge)> 1e-6 )bob.error("domain is charged");

  char fname[filename_size];

 for( int j=0; j<nsp; j++ )
 {

  sprintf(fname,"%s/domain-%d", path, j+1);

  ofstream domain_file(fname);
  if (!domain_file) bob.error("cannot open output file: ", fname );

  domain_file.precision( 3 );
  domain_file.setf( ios::showpoint );
  domain_file.setf( ios::scientific );

  domain_file << setw(6)  << "# cell"
	      << setw(12) << "x"
	      << setw(12) << "rho_el."
	      << setw(12) << "rho_ion"
	      << setw(7)  << "n_el"
	      << setw(7)  << "n_ion" << endl;

  for( cell=Lbuf; cell!=dummy; cell=cell->next )
    {
      domain_file << setw(6)  << cell->number
		  << setw(12) << cell->x
		  << setw(12) << cell->dens_e[j] * input.n_el_over_nc[j]
		  << setw(12) << cell->dens_i[j] * input.n_ion_over_nc[j]
		  << setw(7)  << cell->np_e[j]
		  << setw(7)  << cell->np_i[j] << endl;
    }

  domain_file.close();
}
}


//////////////////////////////////////////////////////////////////////////////////////////


void domain::count_particles()
  // counts particles in domain -> n_el, n_ion, n_part
{
  static error_handler bob("domain::count_particles",errname);
/*
  struct cell *cell;
  struct particle *part;

  int nparts_1, nparts_2;
  int n_el_old   = n_el;       // keep in mind the old numbers
  int n_ion_old  = n_ion;
  int n_part_old = n_part;

  n_el   = 0;
  n_ion  = 0;
  n_part = 0;

  for( cell=Lbuf; cell!=dummy; cell=cell->next )
    {
      nparts_1 = nparts_2 = 0;

      if (cell->npart != 0) {

	nparts_1 += cell->npart;

	for( part=cell->first; part!=NULL; part=part->next )
	  {
	    nparts_2++;

	    if (part->species==0) n_el  ++;
	    else                  n_ion ++;
	    n_part ++;
	  }

	if (nparts_1 != nparts_2) {
	  bob.message( "particle numbers incorrect" );
	  bob.message( "                 in cell:", cell->number );
	  bob.message( "             cell_parts =", nparts_1 );
	  bob.message( "                  parts =", nparts_2 );

	  bob.error("");
	}

      }
    }

  bob.message( "particle numbers", n_el, n_ion, n_part );

  if (n_el!=n_el_old || n_ion!=n_ion_old || n_part!=n_part_old)
    {
      bob.message( "old particle numbers", n_el_old, n_ion_old, n_part_old );
      bob.error("");
    }*/
}

//////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////
//eof
