#ifndef DIAGNOSTIC_SPACETIME_H
#define DIAGNOSTIC_SPACETIME_H
 
#include "diagnostic_stepper.h"
#include "common.h"
#include "error.h"
#include "parameter.h"
#include "domain.h"
#include "matrix.h"
#include <cmath>

using namespace std;


class input_spacetime {
private:
  char          errname[filename_size];
  readfile      rf;
  void          save( parameter &p );

public:
  stepper_param stepper_de, stepper_di,
                stepper_jx, stepper_jy, stepper_jz,
                stepper_ex, stepper_ey, stepper_ez, 
                stepper_bx, stepper_by, stepper_bz, 
                stepper_edens;
  int           Q_restart;
  int           x_skip,t_skip;
  double        Beta,Gamma;

  char          restart_file[filename_size];

  input_spacetime( parameter &p );
};

//////////////////////////////////////////////////////////////////////////////////////////

class spacetime {

private:
  char            errname[filename_size];
  readfile        rf;
  input_spacetime input;

public:
  int             output_period_de, output_period_di,  
                  output_period_jx, output_period_jy, output_period_jz, 
                  output_period_ex, output_period_ey, output_period_ez, 
                  output_period_bx, output_period_by, output_period_bz, 
                  output_period_edens; 

  diagnostic_stepper stepper_de, stepper_di,
                     stepper_jx, stepper_jy, stepper_jz, 
                     stepper_ex, stepper_ey, stepper_ez,
                     stepper_bx, stepper_by, stepper_bz, 
                     stepper_edens;
  
  char *name_de, *name_di,
       *name_jx, *name_jy, *name_jz, 
       *name_ex, *name_ey, *name_ez, 
       *name_bx, *name_by, *name_bz, 
       *name_edens;
  
  struct cell  *x_start_cell, *x_stop_cell;

  spacetime            ( parameter &p );
  void boundaries      ( struct cell **cell_start, struct cell **cell_stop, int *x_steps, 
			 diagnostic_stepper *stepper, domain *grid );
  void write_de        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_di        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_jx        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_jy        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_jz        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_ex        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_ey        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_ez        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_bx        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_by        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_bz        ( double time, domain *grid, int time_out_count, int ok, parameter &p );
  void write_edens     ( double time, domain *grid, int time_out_count, int ok, parameter &p );
};

//////////////////////////////////////////////////////////////////////////////////////////

#endif



