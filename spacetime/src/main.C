/***********************************************************************
 * 
 * 
 * ulimit -s unlimited
 *
//////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <TString.h>
#include <TH2.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TStyle.h>
#include <TPad.h>
#include <TColor.h>
#include <TGaxis.h>
#include <TLatex.h>
#include <TExec.h>
#include <TSystem.h>
using namespace std;
void Pal(void);
void Pal_zero(void);
string process= ".L ./Dynamic.C+";




int main(int argc, char *argv[])
{	
	string species;
	double xmin, xmax, tmin, tmax;
	int tfmin, tfmax;
	int cpw;
	if(argc == 7){
		cout<<"  MLPIC spacetime plotting -> "<<endl;
		species = argv[1];
		cpw = atoi(argv[2]);
		xmin = atof(argv[3]);
		xmax = atof(argv[4]);
		if(xmin > xmax){
			double tempx = xmin;
			xmin = xmax;
			xmax = tempx;
		}		
		tmin = atof(argv[5]);
		tmax = atof(argv[6]);				
		if(tmin > tmax){
			double tempt = tmin;
			tmin = tmax;
			tmax = tempt;
		}
		tfmin = floor(tmin);
		tfmax = floor(tmax-0.001);
	}else{
		cout<<"input wrong"<<endl;
		cout<<"Usage: mlpic-spacetime [species] [cpw] [xmin] [xmax] [tmin] [tmax] "<<endl;
		return 0;		
	}
	//open the file and get the data
	FILE  *file;
	char infname[50];
	double x_start_in, x_stop_in, time;
	int x_steps_in, period_in;
	int t_steps = 0;
	//open one file and get some data
	sprintf( infname, "./spacetime-%s-%d", species.c_str(), tfmin );
	file = fopen( infname, "rb" );
	if(!file){
		cout<<"Cannot open "<<infname<<endl;
		return 0;
	}
	fread( &period_in, sizeof(int), 1, file );
	fread( &x_start_in, sizeof(double), 1, file );
	fread( &x_stop_in, sizeof(double), 1, file );
	fread( &x_steps_in, sizeof(int), 1, file );
	float matrix_int[x_steps_in];
	while( fread( &time, sizeof(double), 1, file ) == 1 )
	{
		t_steps++;
		fread( &matrix_int, sizeof(float), x_steps_in, file );
	}
	fclose(file);
	float dt = 1.0/(float)t_steps;
	int tbin = t_steps * (tfmax - tfmin + 1);
	int xbin = x_steps_in;
	if(xmin*cpw > xbin || xmax*cpw > xbin)
	{
		cout<<"CPW or XRange is wrong"<<endl;
		return 0;
	}
	float data[tbin][xbin];
	int count = 0;
	for( int period = tfmin; period <= tfmax; period++ )
	{
		sprintf( infname, "./spacetime-%s-%d", species.c_str(), period );
		file = fopen( infname, "rb" );
		if(!file){
			cout<<"Cannot open "<<infname<<endl;
			return 0;
		}
		fread( &period_in, sizeof(int), 1, file );
		if( period != period_in ) cout<<"wrong file"<<endl;
		fread( &x_start_in, sizeof(double), 1, file );
		fread( &x_stop_in, sizeof(double), 1, file );
		fread( &x_steps_in, sizeof(int), 1, file );
		float matrix_read[x_steps_in];
		while( fread( &time, sizeof(double), 1, file ) == 1 )
		{
			fread( &matrix_read, sizeof(float), x_steps_in, file );
			for( int x_count = 0; x_count < x_steps_in; x_count++ ) 
			data[count][x_count] = matrix_read[x_count];
			count++;
		}
		fclose(file);
	}
	//plot///////////////////////////////////////////////////////////// 
	TApplication* rootapp = new TApplication("spacetime",&argc, argv);	
	TGaxis::SetMaxDigits(3);
	gStyle->SetTitleSize(0.065,"t");
	gStyle->SetOptStat(0);
	
	gStyle->SetNumberContours(99);	
	char ccname[50];
	sprintf( ccname, "spacetime-%s", species.c_str());
	TCanvas *cc = new TCanvas("cc",ccname,10,10,800,800);
	gPad->SetMargin(0.15,0.15,0.15,0.1);
	cc->ToggleEventStatus();
	
	int tbin_num = (tmax - tmin)*(float)t_steps;
	int tbin_bg = (tmin - tfmin)*(float)t_steps;
	int xbin_num = (xmax - xmin)*cpw;
	int xbin_bg = xmin*cpw;
	cout<<"  BinSize: "<<xbin_num<<" * "<<tbin_num<<endl;
	TH2F *h = new TH2F("h","",xbin_num,xmin,xmax,tbin_num,tmin,tmax);
	double_t max_data, min_data;
	double_t z_max, z_min;
	max_data = max_data =0.0;
	z_max = z_min = 0.0;
	for(int j=0;j<tbin_num;j++)
	for(int i=0;i<xbin_num;i++)
	{
		double_t ijdata = data[j+tbin_bg][i+xbin_bg];
		h->SetBinContent(i,j,ijdata); 
		//max
		if(ijdata > max_data) max_data = ijdata;
		//min
		if(ijdata < min_data) min_data = ijdata;
	}
	if(min_data < 0.0)
	{
		z_max = max(max_data,abs(min_data));
		z_min = -1.0 * z_max;	
		Pal_zero();
	}else{
		z_min = min_data;
		z_max = max_data;
		Pal();
	}
	
	h->Draw("colz");
	h->SetTitle(ccname);	
	h->GetXaxis()->SetTitle("X/#lambda");
	h->GetXaxis()->CenterTitle();	
	h->GetXaxis()->SetTitleSize(0.06);
	h->GetXaxis()->SetTitleOffset(1.1);
	h->GetXaxis()->SetLabelSize(0.06);
	h->GetXaxis()->SetLabelOffset(0.025);
	h->GetXaxis()->SetNdivisions(6,2,0,kTRUE);
	h->GetYaxis()->SetTitle("t/T");
	h->GetYaxis()->CenterTitle();	
	h->GetYaxis()->SetTitleSize(0.06);
	h->GetYaxis()->SetTitleOffset(1.1);
	h->GetYaxis()->SetLabelSize(0.06);
	h->GetYaxis()->SetLabelOffset(0.025);
	h->GetYaxis()->SetNdivisions(6,2,0,kTRUE);
	h->GetZaxis()->SetLabelSize(0.05);
	h->GetZaxis()->SetLabelOffset(0.01);
	h->GetZaxis()->SetNdivisions(8,2,0,kTRUE);
	h->GetZaxis()->SetRangeUser(z_min,z_max);

	
	char savename[50];
	sprintf( savename, "spacetime-%s.gif", species.c_str());
	cc->SaveAs(savename);
	gROOT->ProcessLine(process.c_str());
	cc->AddExec("dynamic","Dynamic()");	
	rootapp->Run();
	return 1;
}

void Pal(void)
{
   const int NCont = 99;		
   static Int_t  colors[NCont];
   static Bool_t initialized = kFALSE;
	const int NRGBs = 5;
	Double_t Red[NRGBs]    = { 1.00, 0.00, 0.00, 1.00, 1.00};
	Double_t Green[NRGBs]  = { 1.00, 0.00, 1.00, 1.00, 0.00};
	Double_t Blue[NRGBs]   = { 1.00, 1.00, 1.00, 0.00, 0.00};
	Double_t Length[NRGBs] = { 0.00, 0.03, 0.1, 0.4,  1.00 };
	if(!initialized){
		Int_t FI = TColor::CreateGradientColorTable(NRGBs,Length,Red,Green,Blue,NCont);
		for (int i=0; i<NCont; i++) colors[i] = FI+i;
		initialized = kTRUE;
		return;
	}
	gStyle->SetPalette(NCont,colors);
}
	
void Pal_zero(void)
{
   const int NCont = 99;		
   static Int_t  colors[NCont];
   static Bool_t initialized = kFALSE;
	const int NRGBs = 5;
	Double_t Red[NRGBs]    = { 0.00, 0.00, 1.00, 1.00, 1.00};
	Double_t Green[NRGBs]  = { 0.00, 1.00, 1.00, 1.00, 0.00};
	Double_t Blue[NRGBs]   = { 1.00, 1.00, 1.00, 0.00, 0.00};
	Double_t Length[NRGBs] = { 0.00, 0.25, 0.5, 0.75,  1.00 };
	if(!initialized){
		Int_t FI = TColor::CreateGradientColorTable(NRGBs,Length,Red,Green,Blue,NCont);
		for (int i=0; i<NCont; i++) colors[i] = FI+i;
		initialized = kTRUE;
		return;
	}
	gStyle->SetPalette(NCont,colors);
}


